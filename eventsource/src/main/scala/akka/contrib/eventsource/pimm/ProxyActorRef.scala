package akka.contrib.eventsource.pimm

import akka.actor.MinimalActorRef

trait ProxyActorRef extends MinimalActorRef {

}
