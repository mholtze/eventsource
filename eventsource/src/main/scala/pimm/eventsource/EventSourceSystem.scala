package pimm.eventsource

import akka.actor._
import com.typesafe.config.Config
import pimm.eventsource.aggregate._
import pimm.eventsource.events._


object EventSourceSystem extends ExtensionId[EventSourceSystem] with ExtensionIdProvider {
  override def lookup = EventSourceSystem
  override def createExtension(system: ExtendedActorSystem) = new EventSourceSystem(system)
  /** Java API: retrieve the Count extension for the given system. */
  override def get(system: ActorSystem): EventSourceSystem = super.get(system)
}


// TODO add support for global snapshots

/** [[ActorSystem]] extension to simplify event sourcing and aggregate roots. */
class EventSourceSystem(system: ExtendedActorSystem) extends Extension with AggregateRefProvider {
  private val settings = EventSourceSettings(system)

  val eventuals = system.actorOf(settings.eventualsProps, "eventuals")
  val aggregateRouter = system.actorOf(settings.aggregateRouterProps, "aggregateRouter")
  val snapshotStrategy: SnapshotStrategy = settings.snapshotStrategy

  override protected def actorRefProvider: ActorRefProvider = system.provider
}

private[eventsource] class EventSourceSettings(actorSystem: ActorSystem, systemConfig: Config) {
  val eventualsProps = new EventualsFactory(actorSystem, systemConfig).createProps()
  val aggregateRouterProps = new AggregateRouterFactory(actorSystem, systemConfig).createProps()
  def snapshotStrategy = SnapshotStrategy.fromConfig(actorSystem, systemConfig)
}
private[eventsource] object EventSourceSettings {
  def apply(actorSystem: ActorSystem) = new EventSourceSettings(actorSystem, actorSystem.settings.config)
}


/** Trait for actors the interact with the event source system that sets of the [[EventSourceSystem]] extension. */
trait EventSourcedActor extends Actor {
  val eventSystem = EventSourceSystem(context.system)
}
