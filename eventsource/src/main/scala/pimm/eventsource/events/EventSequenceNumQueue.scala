package pimm.eventsource.events

import java.time.Instant
import java.util.UUID

import scala.annotation.tailrec

private[events] object EventSequenceNumQueue {
  case class PendingEvent(
    seqNum: Long,
    expires: Long,
    envelope: PersistentEventEnvelope,
    var acknowledged: Boolean = false,
    var timedOut: Boolean = false)

  object PendingEvent {
    def applyTimeout(seqNum: Long, envelope: PersistentEventEnvelope, timeout: Long) = new PendingEvent(
      seqNum,
      Instant.now().toEpochMilli + timeout,
      envelope
    )
  }
}

private[events] class EventSequenceNumQueue(ackTimeout: Long) {
  import EventSequenceNumQueue._

  private var _pendingCorrelations = Map[Long, PendingEvent]()
  private var _pendingSeqNumbers = Vector[PendingEvent]()

  private[events] def pendingEvents = _pendingCorrelations
  private[events] def pendingSeqNumbers() = _pendingSeqNumbers

  def size: Int = _pendingSeqNumbers.size

  def enqueuePending(env: PersistentEventEnvelope): Unit = {
    val pending = PendingEvent.applyTimeout(env.globalSequenceNr, env, ackTimeout)
    _pendingSeqNumbers :+= pending
    _pendingCorrelations += env.globalSequenceNr -> pending
  }

  def acknowledge(globalSeqNr: Long): Unit = {
    val pending = _pendingCorrelations.get(globalSeqNr)
    pending.foreach(n => {
      _pendingCorrelations -= globalSeqNr
      n.acknowledged = true
    })
  }

  def dequeueMax(): Option[Long] = {
    @tailrec
    def maxAck(pending: Vector[PendingEvent], maxDequeued: Option[Long]): (Option[Long], Vector[PendingEvent]) = {
      pending.headOption match {
        case None => (maxDequeued, pending)
        case Some(p) if !p.acknowledged => (maxDequeued, pending)
        case Some(p) => maxAck(pending.tail, Some(p.seqNum))
      }
    }
    val (maxDequeued, pending) = maxAck(_pendingSeqNumbers, None)

    _pendingSeqNumbers = pending
    maxDequeued
  }

  def checkTimeouts(): Seq[PersistentEventEnvelope] = {
    val now = Instant.now().toEpochMilli
    val timeouts = _pendingSeqNumbers.view
      .takeWhile(_.expires < now)
      .filter(p => !p.timedOut && !p.acknowledged)
      .toList

    timeouts.foreach(p => {
      p.timedOut = true
    })
    timeouts.map(_.envelope)
  }
}