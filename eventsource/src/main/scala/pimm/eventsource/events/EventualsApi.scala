package pimm.eventsource.events

import akka.stream.scaladsl.Source

import scala.concurrent.Future


trait EventualsApi {
  def readLastGlobalSeqNr(): Future[Long]
  def saveLastGlobalSeqNr(lastSeqNr: Long): Unit
  def allEventsLive(globalFrom: Long): Source[PersistentEventEnvelope, Unit]
}