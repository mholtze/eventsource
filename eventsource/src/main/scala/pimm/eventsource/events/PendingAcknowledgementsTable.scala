package pimm.eventsource.events

import java.time.Instant

import akka.actor.Status

import scala.collection.immutable
import scala.collection.mutable


class AcknowledgedTimeoutException(
  val result: Any,
  val timeout: Option[AckTimeout],
  message: String = "Consistency acknowledgement timeout.",
  cause: Throwable = null
) extends RuntimeException(message, cause)


private[eventsource] object PendingAcknowledgementsTable {
  class Entry(val askConsistent: AcknowledgeConsistent, val expires: Long) {
    private val _received = mutable.HashMap.empty[Long, Boolean] ++=
      0 until askConsistent.numEvents map(offset => (askConsistent.startSequenceNr+offset.toLong, false))

    private var _pendingCount = _received.size
    private[eventsource] val endSequenceNr = askConsistent.startSequenceNr + askConsistent.numEvents.toLong - 1L

    @inline private[eventsource] def startSequenceNr = askConsistent.startSequenceNr
    @inline private[eventsource] def received = _received.toMap
    @inline private[eventsource] def pendingCount = _pendingCount

    def receive(sequenceNr: Long): Boolean = {
      if (contains(sequenceNr)) {
        if (!received(sequenceNr)) {
          _received += sequenceNr -> true
          _pendingCount -= 1
        }
        pendingCount == 0
      }
      else false
    }

    def contains(sequenceNr: Long): Boolean =
      sequenceNr >= startSequenceNr && sequenceNr <= endSequenceNr
  }

  implicit object EntryOrdering extends Ordering[Entry] {
    def compare(a:Entry, b:Entry) = a.startSequenceNr compare b.startSequenceNr
  }
}

private[eventsource] class PendingAcknowledgementsTable(entryTimeout: Long) {
  import PendingAcknowledgementsTable._

  private var _results = Map.empty[String, immutable.TreeSet[Entry]]

  @inline private[eventsource] def results = _results

  def add(askConsistent: AcknowledgeConsistent): Unit = {
    val entry = new Entry(askConsistent, Instant.now().toEpochMilli + entryTimeout)
    val updated = results.get(askConsistent.aggregateId) match {
      case Some(entries) => entries + entry
      case None => immutable.TreeSet[Entry](entry)
    }
    _results += askConsistent.aggregateId -> updated
  }

  def receiveEvent(aggregateId: String, sequenceNr: Long): Unit = {
    for (entries <- results.get(aggregateId)) {
      val completed = entries.filter(_.receive(sequenceNr))
      for (c <- completed)
        c.askConsistent.sender ! Status.Success(c.askConsistent.result)

      updateRemaining(aggregateId, entries -- completed)
    }
  }

  def receiveTimeout(timeout: AckTimeout): Unit = {
    val aggregateId = timeout.envelope.aggregateId
    val sequenceNr = timeout.envelope.sequenceNr

    for (entries <- results.get(aggregateId)) {
      val timeouts = entries.filter(_.contains(sequenceNr))
      for (t <- timeouts)
        t.askConsistent.sender ! Status.Failure(new AcknowledgedTimeoutException(t.askConsistent.result, Some(timeout)))

      updateRemaining(aggregateId, entries -- timeouts)
    }
  }

  def checkTimeouts(): Unit = {
    val now = Instant.now().toEpochMilli

    for (aggregateId <- results.keys) {
      val entries = results(aggregateId)
      val timeouts = entries.filter(_.expires < now)
      for (t <- timeouts)
        t.askConsistent.sender ! Status.Failure(new AcknowledgedTimeoutException(t.askConsistent.result, None))

      updateRemaining(aggregateId, entries -- timeouts)
    }
  }

  private def updateRemaining(aggregateId: String, remaining: immutable.TreeSet[Entry]): Unit = {
    if (remaining.isEmpty)
      _results -= aggregateId
    else
      _results += aggregateId -> remaining
  }
}