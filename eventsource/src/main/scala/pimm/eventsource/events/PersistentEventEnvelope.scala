package pimm.eventsource.events

/** Identifier used to by eventuals and acknowledgements to match event envelopes. */
trait PersistentEventEnvelopeId {
  def aggregateId: String
  def sequenceNr: Long
  def globalSequenceNr: Long
}

trait PersistentEventEnvelope extends PersistentEventEnvelopeId {
  def event: Any
}
