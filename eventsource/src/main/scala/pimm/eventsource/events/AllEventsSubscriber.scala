package pimm.eventsource.events

import akka.actor.ActorRef
import akka.stream.actor.{ActorSubscriberMessage, MaxInFlightRequestStrategy, ActorSubscriber}

private[eventsource] object AllEventsSubscriber {
  sealed trait AllEventsSubscriberMessage
  final case class DecrementPending(value: Int) extends AllEventsSubscriberMessage
  final case object QueryPending extends AllEventsSubscriberMessage

  final case class PendingCount(count: Int)
}

private[eventsource] class AllEventsSubscriber(
  val consumer: ActorRef,
  val maxPending: Int,
  var pendingCount: Int) extends ActorSubscriber {

  import AllEventsSubscriber._
  import Eventuals._

  consumer ! AllEvents(self, pendingCount)

  override val requestStrategy = new MaxInFlightRequestStrategy(max = maxPending) {
    override def batchSize: Int = 1

    override def inFlightInternally: Int =
      pendingCount
  }

  def receive: Receive = {
    case DecrementPending(value) =>
      pendingCount -= value
    case QueryPending =>
      consumer ! PendingCount(pendingCount)
    case ActorSubscriberMessage.OnNext(e: PersistentEventEnvelope) =>
      pendingCount += 1
      consumer ! e
    case ActorSubscriberMessage.OnComplete => consumer ! new AllEventsException("The all events stream stopped unexpectedly")
    case ActorSubscriberMessage.OnError(cause) => consumer ! new AllEventsException("The all events stream failed", cause)
  }
}
