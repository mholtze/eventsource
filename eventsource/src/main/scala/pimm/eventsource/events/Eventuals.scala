package pimm.eventsource.events

import akka.actor._
import akka.event.{Logging, LoggingAdapter}
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.Sink
import com.typesafe.config.Config
import scala.concurrent.duration._
import scala.concurrent.Await


object Eventuals {
  class AllEventsException(message: String = null, cause: Throwable = null) extends RuntimeException(message, cause)
  class ConsistencyBuilderException(message: String = null, cause: Throwable = null) extends RuntimeException(message, cause)

  private [eventsource] object StartEventuals

  sealed trait EventualsMessage
  private[eventsource] case object CheckTimeouts extends EventualsMessage
  private[eventsource] case object ConnectToAllEvents extends EventualsMessage
  private[eventsource] final case class AllEvents(subscriber: ActorRef, pendingCount: Int) extends EventualsMessage

  final case class EventAck(envelope: PersistentEventEnvelopeId) extends EventualsMessage
  final case class EventAckSeq(envelopes: Seq[PersistentEventEnvelopeId]) extends EventualsMessage
  case object ResendPending extends EventualsMessage

  case class ResendEvent(event: Any)
  case object ResendComplete

  type ConsistencyBuilderPropsFunction = (ActorRef) => Props
  type ImplFunction = (ActorSystem, ActorRef) => EventualsApi
}

final case class AckTimeout(envelope: PersistentEventEnvelope)
  extends DeadLetterSuppression with NoSerializationVerificationNeeded


final case class AcknowledgeConsistent(
  aggregateId: String,
  startSequenceNr: Long,
  numEvents: Int,
  result: Any,
  sender: ActorRef)

// TODO: change to pull all default values from config
class Eventuals(
  implFunction: Eventuals.ImplFunction,
  consistencyBuilderProps: Eventuals.ConsistencyBuilderPropsFunction = NoConsistencyBuilder.props,
  ackTimeout: Long = 5.seconds.toMillis,
  askConsistentTimeout: Long = 5.seconds.toMillis,
  allEventsRetryDelay: Long = 1.seconds.toMillis,
  lastGlobaSeqNrTimeout: Long = 2.seconds.toMillis,
  maxPendingEvents: Int = 1000
) extends Actor {
  import Eventuals._
  import AllEventsSubscriber.DecrementPending
  import context.dispatcher // for ExecutionContext used by scheduler

  private implicit val materializer = ActorMaterializer()

  val log = Logging(context.system, this)
  val impl = implFunction(context.system, self)
  val consistencyBuilder = context.actorOf(consistencyBuilderProps(self), "consistencyBuilder")
  var lastGlobalSeqNr: Long = 0L
  val eventsSeqNumQueue = new EventSequenceNumQueue(ackTimeout)
  val pendingAcks = new PendingAcknowledgementsTable(askConsistentTimeout)
  var allEvents: Option[ActorRef] = None

  self ! StartEventuals

  override def receive: Receive = starting

  def starting: Receive = {
    case StartEventuals =>
      lastGlobalSeqNr = Await.result(impl.readLastGlobalSeqNr(), lastGlobaSeqNrTimeout.milli)
      self ! ConnectToAllEvents
      context.system.scheduler.scheduleOnce(ackTimeout.millis, self, CheckTimeouts)
      context.become(looping)
  }

  def looping: Receive = {
    case ConnectToAllEvents =>
      impl.allEventsLive(lastGlobalSeqNr+1).runWith(Sink.actorSubscriber(
        Props(new AllEventsSubscriber(self, maxPendingEvents, eventsSeqNumQueue.size))))
      ()
    case AllEvents(subscriber, pendingCount) =>
      allEvents = Some(subscriber)
      // get the decrement to sync up with any acknowledged events between subscribe and now
      updateAllEventsPending(pendingCount)
    case err: AllEventsException =>
      log.error(err.getCause, "Failure receiving events from the all-events stream.")
      allEvents = None
      context.system.scheduler.scheduleOnce(allEventsRetryDelay.millis, self, ConnectToAllEvents)
      ()

    case env: PersistentEventEnvelope =>
      receiveGlobalEventEnvelope(env)
    case EventAck(a) =>
      acknowledgeEvent(a)
      persistLastGlobalSeqNr()
    case EventAckSeq(acks) =>
      acks.foreach(acknowledgeEvent)
      persistLastGlobalSeqNr()
    case askConsistent: AcknowledgeConsistent =>
      pendingAcks.add(askConsistent)
    case CheckTimeouts =>
      context.system.scheduler.scheduleOnce(ackTimeout.millis, self, CheckTimeouts)
      publishTimeouts(eventsSeqNumQueue.checkTimeouts())
      // after processing seq number timeouts, deal with any consistency ack timeouts
      pendingAcks.checkTimeouts()
    case ResendPending =>
      publishAllPending()
  }

  private def receiveGlobalEventEnvelope(env: PersistentEventEnvelope): Unit = {
    consistencyBuilder ! env
    lastGlobalSeqNr = env.globalSequenceNr
    eventsSeqNumQueue.enqueuePending(env)
  }

  private def acknowledgeEvent(envId: PersistentEventEnvelopeId): Unit = {
    eventsSeqNumQueue.acknowledge(envId.globalSequenceNr)
    pendingAcks.receiveEvent(envId.aggregateId, envId.sequenceNr)
  }

  private def publishTimeouts(timeouts: Seq[PersistentEventEnvelope]): Unit = {
    timeouts.foreach(timeout => {
      val ackTimeout = AckTimeout(timeout)
      pendingAcks.receiveTimeout(ackTimeout)
      consistencyBuilder ! ackTimeout
    })
  }

  private def publishAllPending(): Unit = {
    for (p <- eventsSeqNumQueue.pendingSeqNumbers() if !p.acknowledged) {
      consistencyBuilder ! ResendEvent(if (p.timedOut) AckTimeout(p.envelope) else p.envelope)
    }
    consistencyBuilder ! ResendComplete
  }

  private def persistLastGlobalSeqNr(): Unit = {
    val currentSize = eventsSeqNumQueue.size
    val seqNrToSave = eventsSeqNumQueue.dequeueMax()
    seqNrToSave.foreach(n => impl.saveLastGlobalSeqNr(n))
    updateAllEventsPending(currentSize)
  }

  private def updateAllEventsPending(sizeBefore: Int): Unit = {
    val decrement = sizeBefore - eventsSeqNumQueue.size
    if (decrement > 0) {
      for (ref <- allEvents) {
        ref ! DecrementPending(decrement)
      }
    }
  }
}

object EventualsFactory {
  class ConsistencyBuilderFunction(val consistencyBuilderType: Class[_]) extends Function1[ActorRef, Props] {
    override def apply(eventuals: ActorRef): Props = Props(consistencyBuilderType, eventuals)
  }
}
class EventualsFactory(actorSystem: ActorSystem, systemConfig: Config) {
  import EventualsFactory._

  val config = systemConfig.getConfig("pimm.eventsource.events.eventuals")

  def createProps(): Props = {
    val impl = createApiImpl()
    createPropsForImpl(impl)
  }

  def createPropsForImpl(impl: Eventuals.ImplFunction): Props = {
    val ackTimeout = config.getDuration("ack-timeout").toMillis
    val askConsistentTimeout = config.getDuration("ask-consistent-timeout").toMillis
    val lastGlobalSeqNrTimeout = config.getDuration("last-global-seqnr-timeout").toMillis
    val allEventRetryDelay = config.getDuration("all-events-retry-delay").toMillis
    val maxPendingEvents = config.getInt("max-pending-events")
    val consistencyBuilderProps = createConsistencyBuilderProps()

    Props(new Eventuals(
      impl,
      consistencyBuilderProps,
      ackTimeout,
      askConsistentTimeout,
      allEventRetryDelay,
      lastGlobalSeqNrTimeout,
      maxPendingEvents))
  }

  def createApiImpl(): Eventuals.ImplFunction = {
    val apiClassName = config.getString("api-class")
    val apiType = Class.forName(apiClassName)
    val apiCons = apiType.getConstructor(classOf[ActorSystem], classOf[ActorRef])

    (as, eventuals) =>
      apiCons.newInstance(as, eventuals).asInstanceOf[EventualsApi]
  }

  def createConsistencyBuilderProps(): Eventuals.ConsistencyBuilderPropsFunction = {
    val consistencyBuilderClassName = config.getString("consistency-builder-class")
    val consistencyBuilderType = Class.forName(consistencyBuilderClassName)

    new ConsistencyBuilderFunction(consistencyBuilderType)
  }
}