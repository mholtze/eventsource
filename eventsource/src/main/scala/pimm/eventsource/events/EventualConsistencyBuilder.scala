package pimm.eventsource.events

import akka.actor.SupervisorStrategy.Escalate
import akka.actor._
import akka.pattern.pipe
import pimm.eventsource.EventSourceSystem
import pimm.eventsource.events.Eventuals.EventAck

import scala.concurrent.Future

// TODO: add testing for this compoment
//       add more documentation about configuring failure handling (in supervisor)

trait EventualConsistencyBuilder extends Actor with Stash {
  import pimm.eventsource.events.Eventuals._

  val eventuals: ActorRef
  val eventSystem = EventSourceSystem(context.system)
  var highestSeqNr = 0L

  eventuals ! ResendPending

  override val supervisorStrategy =
    // always escalate child failures to cause the builder to be restarted
    OneForOneStrategy() {
      case _ => Escalate
    }

  override def receive = starting

  def starting: Receive = {
    case ResendEvent(env: PersistentEventEnvelope) => receiveEnvelope(env)
    case ResendEvent(timeout: AckTimeout) => receiveEnvelope(timeout.envelope)
    case ResendComplete =>
      unstashAll()
      context.become(looping)
    case Status.Failure(cause) =>
      throw new ConsistencyBuilderException("Consistency processing failure", cause)
    case _ => stash()
  }

  def looping: Receive = {
    case env: PersistentEventEnvelope => receiveEnvelope(env)
    case ack: EventAck => eventuals ! EventAck(ack.envelope)
    case AckTimeout(env) => receiveAckTimeout(env)
  }

  def receiveAckTimeout(env: PersistentEventEnvelope): Unit = {
    // just throw an exception to force the actor to restart
    val msg = s"[${env.event.getClass.getCanonicalName}] timeout before processing could be completed."
    throw new ConsistencyBuilderException(msg)
  }

  final def receiveEnvelope(env: PersistentEventEnvelope): Unit = {
    // make sure we skip any events that were both resent and stashed
    if (env.globalSequenceNr > highestSeqNr) {
      highestSeqNr = env.globalSequenceNr
      onEnvelope(env)
    }
  }

  def onEnvelope(env: PersistentEventEnvelope): Unit
}

trait AsyncEventualConsistencyBuilder extends EventualConsistencyBuilder {
  import context.dispatcher

  /** Handler for envelopes. The default implementation is to call [[handleAsync]] and wait for the returned
    * [[Future]] to complete.
    */
  def onEnvelope(env: PersistentEventEnvelope): Unit = {
    val future = handleAsync(env).applyOrElse[Any, Future[EventAck]](env.event, x => Future.successful(EventAck(env)))
    future pipeTo self
    ()
  }

  def handleAsync(env: PersistentEventEnvelope): PartialFunction[Any, Future[EventAck]]
}

/** Default consistency builder that immediately acknowledges all events. */
final class NoConsistencyBuilder(val eventuals: ActorRef) extends Actor {
  override def receive = {
    case env: PersistentEventEnvelope => eventuals ! EventAck(env)
  }
}

object NoConsistencyBuilder {
  def props(eventuals: ActorRef) = Props(new NoConsistencyBuilder(eventuals))
}

