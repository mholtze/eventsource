package pimm.eventsource.aggregate

import akka.actor.{Status, ActorRef}
import akka.event.Logging
import akka.persistence._
import com.google.common.base.Throwables
import pimm.eventsource.{aggregate, EventSourceSystem}
import pimm.eventsource.events.AcknowledgeConsistent

import scala.collection.Iterable
import scala.reflect.ClassTag


class AggregateException(message: String = null, cause: Throwable = null) extends RuntimeException(message, cause)


object AggregateActor {
  import CommandRequest._

  case object Shutdown
  private case object SaveSnapshot

  case class SnapshotAttempt(firstAttemptTime: Long, lastAttempTitme: Long, attemptNumber: Int)

  object SnapshotAttempt {
    def apply(): SnapshotAttempt = {
      val now = System.currentTimeMillis()
      SnapshotAttempt(now, now, 1)
    }
    def apply(lastAttempt: SnapshotAttempt): SnapshotAttempt = {
      val now = System.currentTimeMillis()
      SnapshotAttempt(lastAttempt.firstAttemptTime, now, lastAttempt.attemptNumber+1)
    }
  }

  private final case class AggregateRequest(commandRequest: Request, sender: ActorRef) {
    @inline
    def command = commandRequest.command

    def sendReply[R](resultStatus: Status.Status): Unit =
      commandRequest match {
        case _: ReplyConsistent => sender ! resultStatus
        case _: Reply => sender ! resultStatus
        case _: ReplyUnboxed => resultStatus match {
          case Status.Success(r: CommandReply.Reply[R]) => sender ! r.root
          case f => sender ! f
        }
        case _: ReplyOption => resultStatus match {
          case Status.Success(r: CommandReply.Reply[R]) => sender ! Option(r.root)
          case f => sender ! f
        }
        case _ => // ignore
      }
  }
}

// TODO add tests for sequence number consistency in AggregateActor applyEvent, recovery, and ResultBuilder

/** Base class for actors that implement aggregate roots.
  *
  * Note that this needs to be class and not a trait so that a ClassTag can be applied to the type parameters.
  *
  * @tparam R the type of the publicly available aggregate root
  * @tparam S the type of the internal state representation
  */
abstract class AggregateActor[R: ClassTag, S] extends PersistentActor {
  import AggregateActor._
  import CommandRequest._
  import context.dispatcher // for ExecutionContext used by scheduler

  type ExecuteCommand = PartialFunction[Any, Any]
  type ApplyEvent = PartialFunction[Any, S]
  type ApplyEventFunction = (S, Long) => ApplyEvent

  val log = Logging(context.system, this)
  val eventSystem = EventSourceSystem(context.system)

  val aggregateId: String
  final override def persistenceId: String = aggregateId

  /** [[AggregateRef]] to this aggregate root. */
  final val aggregateSelf = eventSystem.to[R](getClass, aggregateId)

  /** Mutable value used to hold the aggregate state. */
  var state: S

  /** Function to generate public aggregate root. If creating the root object is expensive, it should cached
    * for the current state
    */
  def toRoot(state: S): R

  def updateState(s: S): Unit = {
    state = s
  }

  private var _currentSnapshotSequenceNr: Long = 0L
  private var _currentSnapshotAttempt: Option[SnapshotAttempt] = None

  def currentSnapshotSequenceNr = _currentSnapshotSequenceNr
  def currentSnapshotAttempt = _currentSnapshotAttempt
  def snapshotStrategy: SnapshotStrategy = eventSystem.snapshotStrategy

  /** Tracks in-flight request in [[persistResultAll]]. */
  private var inFlightRequest: AggregateRequest = null

  /** Returns a [[ResultBuilder]] builder for this actor and the current aggregate state. */
  def commandResult: ResultBuilder[S] = new CommandContext(state, lastSequenceNr+1, applyEvent).resultBuilder

  /** Implement this method to handle event messages. */
  def applyEvent(state: S, seqNr: Long): ApplyEvent

  /** Implement this method to handle command messages. */
  def executeCommand: ExecuteCommand

  def aroundApplyEvent(applyEvent: ApplyEventFunction, event: Any, state: S, seqNr: Long): S =
    applyEvent(state, seqNr)(event)

  final def applyEvent(event: Any, state: S, seqNr: Long): S = aroundApplyEvent(applyEvent, event, state, seqNr)

  override def receiveRecover = {
    case SnapshotOffer(metadata, offeredSnapshot) =>
      state = offeredSnapshot.asInstanceOf[S]
      _currentSnapshotSequenceNr = metadata.sequenceNr
    case RecoveryCompleted =>
      if (snapshotStrategy.takeSnapshot(currentSnapshotSequenceNr, lastSequenceNr)) {
        self ! SaveSnapshot
      }
    case e =>
      state = aroundApplyEvent(applyEvent, e, state, lastSequenceNr)
  }

  override def receiveCommand = {
    case Shutdown => context.stop(self)

    case SaveSnapshot => onSaveSnapshot()
    case SaveSnapshotSuccess(metadata) => onSnapshotSaved(metadata)
    case SaveSnapshotFailure(metadata, reason) => onSnapshotFailed(metadata, reason)
    //case DeleteSnapshotFailure => // future: if a snapshot delete policy is added, deal with errors
    //case DeleteSnapshotsFailure => // future: if a snapshot delete policy is added, deal with errors

    case cmdRequest: Request => executeCommandRequest(cmdRequest)
    case cmd => executeCommandRequest(NoReply(cmd))
  }

  private def onSaveSnapshot(): Unit = {
    _currentSnapshotAttempt = Some(_currentSnapshotAttempt match {
      case Some(attempt) => SnapshotAttempt(attempt)
      case None => SnapshotAttempt()
    })
    if (snapshotStrategy.takeSnapshot(currentSnapshotSequenceNr, lastSequenceNr))
      saveSnapshot(state)
  }

  private def onSnapshotSaved(metadata: SnapshotMetadata): Unit = {
    _currentSnapshotAttempt = None
    _currentSnapshotSequenceNr = metadata.sequenceNr
    // future: a snapshot delete policy can be added and applied here
  }

  private def onSnapshotFailed(metadata: SnapshotMetadata, cause: Throwable): Unit = {
    val rootCause = Throwables.getRootCause(cause)
    log.warning("Failed to saveSnapshot given metadata [{}] due to: [{}: {}]",
      metadata, rootCause.getClass.getCanonicalName, rootCause.getMessage)

    val attempt = _currentSnapshotAttempt.getOrElse(SnapshotAttempt())
    snapshotStrategy.retryAfter(attempt) match {
      case Some(duration) =>
        context.system.scheduler.scheduleOnce(duration, self, SaveSnapshot)
        ()
      case None =>
        _currentSnapshotAttempt = None
        log.warning("Save snapshot retries exhausted for metadata [{}]", metadata)
    }
  }

  /** Execute the command contained in the request and apply the appropriate reply based on the request type. */
  private def executeCommandRequest(request: Request): Unit = {
    val result = executeCommandInternal(request.command)
    val aggregateRequest = AggregateRequest(request, sender())
    handleResult(aggregateRequest, result)
  }

  private def handleResult(request: AggregateRequest, result: CommandResult.Result): Unit = result match {
    case f: CommandResult.Failure =>
      val message = s"Command [${request.command.getClass.getCanonicalName}] failed."
      val cmdException = new CommandFailureException(f.toReply(toRoot(state)), message, f.cause)

      val rootCause = Throwables.getRootCause(f.cause)
      log.error(cmdException, "Command [{}] failed due to: [{}: {}]",
        request.command.getClass.getCanonicalName, rootCause.getClass.getCanonicalName, rootCause.getMessage)

      request.sendReply[R](Status.Failure(cmdException))
    case r: CommandResult.Rejected =>
      val message = s"Command [${request.command.getClass.getCanonicalName}] rejected."
      request.sendReply[R](Status.Failure(new CommandRejectedException(r.toReply(toRoot(state)), message, r.cause)))
    case CommandResult.Conflict =>
      val message = s"Command [${request.command.getClass.getCanonicalName}] conflict."
      request.sendReply[R](Status.Failure(new CommandConflictException(CommandResult.Conflict.toReply(toRoot(state)), message)))
    case CommandResult.Root =>
      request.sendReply[R](Status.Success(CommandReply.RootRef[R](toRoot(state))))
    case r: CommandResult.Success[S] =>
      handleCommandSuccess(request, r)
  }

  private def handleCommandSuccess(request: AggregateRequest, result: CommandResult.Success[S]) =
    try {
      request.commandRequest match {
        case _: ReplyConsistent =>
          // give the set of events in the sequence that need a consistency acknowledgement
          val reply = result.toReply(toRoot(result.state))
          eventSystem.eventuals ! AcknowledgeConsistent(aggregateId, lastSequenceNr+1, result.events.size, reply, request.sender)
          persistResultAll(request, result) { () => }
        case _: RequestReply => persistResultAll(request, result) { () =>
          request.sendReply[R](Status.Success(result.toReply(toRoot(state))))
        }
        case _ => persistResultAll(request, result) { () => }
      }
    }
    catch {
      case ex: Throwable => handleResult(request, CommandResult.Failure(ex))
    }

  def aroundExecuteCommand(executeCommand: ExecuteCommand, message: Any): Any =
    executeCommand.applyOrElse[Any, Any](message, {
      case StandardCommands.Root => CommandResult.Root
      case x => CommandResult.Failure(new AggregateException("Unknown command type " + x.getClass.getCanonicalName))
    })

  private def executeCommandInternal(message: Any): CommandResult.Result =
    try {
      val result = aroundExecuteCommand(executeCommand, message)
      // TODO add tests for all these cases
      result match {
        case es: ResultBuilder[R] => es.asResult
        case r: CommandResult.Result => r // Result instance
        case s: Iterable[Any] => commandResult ++ s asResult // list of events
        case s: AnyRef if s.eq(state.asInstanceOf[AnyRef]) => CommandResult.Root // root value
        case e => commandResult + e asResult // single event
      }
    }
    catch {
      case ex: Throwable => CommandResult.Failure( ex)
    }

  /** Wrapper for persistAll that calls the handler only on the last event. */
  final def persistResultAll(request: AggregateRequest, result: CommandResult.Success[S])(handler: () => Unit) = {
    // since the "synchronous-like" version of persistAll is being used, the in-flight request can be tracked
    // using a simple variable and does not require any kind lookup table or queue
    inFlightRequest = request

    var count = result.events.size
    persistAll(result.events){ _=>
      count -= 1
      if (count == 0 && inFlightRequest != null) {
        inFlightRequest = null
        updateState(result.state)
        handler()

        if (snapshotStrategy.takeSnapshot(currentSnapshotSequenceNr, lastSequenceNr)) {
          self ! SaveSnapshot
        }
      }
    }
  }

  private def handleInFlightFailure(message: String, cause: Throwable, event: Any, seqNr: Long): Unit = {
    if (inFlightRequest != null) {
      val fullMessage = s"Command [${inFlightRequest.command.getClass.getCanonicalName}] failed. " + message
      inFlightRequest.sendReply(Status.Failure(new CommandFailureException(CommandReply.Failure[R](toRoot(state), cause), fullMessage, cause)))
      inFlightRequest = null
    }
  }

  override protected def onPersistFailure(cause: Throwable, event: Any, seqNr: Long): Unit = {
    super.onPersistFailure(cause, event, seqNr)
    val message = s"Failed to persist event type [${event.getClass.getCanonicalName}] with sequence number [$seqNr] for aggregateId [$aggregateId]."
    handleInFlightFailure(message, cause, event, seqNr)
  }

  override protected def onPersistRejected(cause: Throwable, event: Any, seqNr: Long): Unit = {
    super.onPersistRejected(cause, event, seqNr)
    // Rejected should behave the same as failure since all event groups are persisted as an atomic unit
    val message = s"Rejected to persist event type [${event.getClass.getCanonicalName}] with sequence number [$seqNr] for aggregateId [$aggregateId]."
    handleInFlightFailure(message, cause, event, seqNr)
  }
}

/** [[AggregateActor]] where the aggregate root maps directly to the internal state. */
abstract class DirectStateAggregate[S: ClassTag] extends AggregateActor[S, S] {
  override def toRoot(state: S): S = state
}
