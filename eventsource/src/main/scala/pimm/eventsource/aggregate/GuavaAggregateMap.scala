package pimm.eventsource.aggregate

import java.util.concurrent.TimeUnit

import akka.actor.{ActorSystem, ActorRef}
import com.typesafe.config.Config
import scala.concurrent.duration._
import com.google.common.cache.{RemovalCause, RemovalNotification, RemovalListener, CacheBuilder}

class GuavaAggregateMap(router: ActorRef, size: Long = 10000L, timeout: Option[Duration] = None) extends AggregateMap {
  import AggregateRouter.ShutdownAggregate

  val cache = {
    val builder = CacheBuilder.newBuilder
      .removalListener(new ActorRemover())
      .maximumSize(size)
      .concurrencyLevel(1)
    timeout.foreach(t => builder.expireAfterAccess(t.toMillis, TimeUnit.MILLISECONDS))
    builder.build[String, ActorRef]()
  }

  override def apply(persistenceId: String): Option[ActorRef] = {
    val actor = cache.getIfPresent(persistenceId)
    if (actor == null) Option.empty else Option.apply(actor)
  }

  override def +=(kv: (String, ActorRef)): Unit = cache.put(kv._1, kv._2)

  override def -=(key: String): Unit = cache.invalidate(key)

  private class ActorRemover extends RemovalListener[String, ActorRef] {
    override def onRemoval(notification: RemovalNotification[String, ActorRef]): Unit = notification.getCause match {
      case RemovalCause.COLLECTED | RemovalCause.EXPIRED | RemovalCause.SIZE =>
        router ! ShutdownAggregate(notification.getKey, Some(notification.getValue))
      case _ => // ignore
    }
  }
}

object GuavaAggregateMap extends AggregateMapFromConfig {
  override def fromConfig(router: ActorRef, actorSystem: ActorSystem, systemConfig: Config): AggregateMap = {
    val config = systemConfig.getConfig("pimm.eventsource.aggregate.guava-map")
    val size = config.getLong("size")
    val timeout = if (config.hasPath("timeout")) Some(config.getDuration("timeout").toMillis) else None

    new GuavaAggregateMap(router, size, timeout.map(_.millis))
  }
}
