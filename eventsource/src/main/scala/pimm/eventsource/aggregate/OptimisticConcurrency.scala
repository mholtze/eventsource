package pimm.eventsource.aggregate

// TODO Add testing

/** Trait that adds optimistic concurrency checks to commands given to an [[AggregateActor]]. */
trait OptimisticConcurrency {
  aggregateActor: AggregateActor[_, _] =>

  import OptimisticConcurrency._

  def concurrencyVersion: Long = lastSequenceNr

  private def doVersionCheck(executeCommand: PartialFunction[Any, Any]): PartialFunction[Any, Any] = {
    case cmd: Versioning if executeCommand.isDefinedAt(cmd) =>
      if (cmd.version == concurrencyVersion) executeCommand
      else CommandResult.Conflict
  }

  def withVersionCheck(executeCommand: PartialFunction[Any, Any]): PartialFunction[Any, Any] =
    doVersionCheck(executeCommand).orElse(executeCommand)
}

object OptimisticConcurrency {
  trait Versioning {
    def version: Long
  }
}