package pimm.eventsource.aggregate

import akka.actor.ActorSystem
import com.typesafe.config.Config

import scala.concurrent.duration._


trait SnapshotStrategy {
  def takeSnapshot(snapshotSequenceNr: Long, lastSequenceNr: Long): Boolean
  def retryAfter(attempt: AggregateActor.SnapshotAttempt): Option[FiniteDuration]
}

trait SnapshotStrategyFromConfig {
  def fromConfig(actorSystem: ActorSystem, systemConfig: Config): SnapshotStrategy
}

object SnapshotStrategy {
  def fromConfig(actorSystem: ActorSystem, systemConfig: Config): SnapshotStrategy = {
    val config = systemConfig.getConfig("pimm.eventsource.aggregate.snapshot-strategy")
    val className = config.getString("class")
    val companionName = className + "$"
    val companionClass = Class.forName(companionName)
    val moduleField = companionClass.getField("MODULE$")
    val module = moduleField.get(null).asInstanceOf[SnapshotStrategyFromConfig]

    module.fromConfig(actorSystem, systemConfig)
  }
}

case class MaxEventsSnapshotStrategy(
  maxEvents: Long,
  retryAttempts: Int,
  retryDelay: FiniteDuration
) extends SnapshotStrategy {

  override def takeSnapshot(snapshotSequenceNr: Long, lastSequenceNr: Long): Boolean =
    lastSequenceNr - snapshotSequenceNr > maxEvents

  override def retryAfter(attempt: AggregateActor.SnapshotAttempt): Option[FiniteDuration] =
    if (attempt.attemptNumber < retryAttempts) Some(retryDelay)
    else None
}

object MaxEventsSnapshotStrategy extends SnapshotStrategyFromConfig {
  override def fromConfig(actorSystem: ActorSystem, systemConfig: Config): SnapshotStrategy = {
    val config = systemConfig.getConfig("pimm.eventsource.aggregate.max-events-snapshot-strategy")
    MaxEventsSnapshotStrategy(
      config.getLong("max-events"),
      config.getInt("retry-attempts"),
      Duration.fromNanos(config.getDuration("retry-delay").toNanos))
  }
}
