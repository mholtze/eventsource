package pimm.eventsource.aggregate

import scala.collection.immutable

class ResultRejectedException(message: String, cause: Throwable = null)
  extends RuntimeException(message, cause)

trait ResultBuilder[S] {
  import CommandResult._

  protected def context: CommandContext[S]
  def applyEvent(event: Any, state: S, seqNr: Long): S = context.applyEvent(event, state, seqNr)

  def state: S

  def events: immutable.IndexedSeq[Any]
  def |>(f: S => Any): ResultBuilder[S]
  def +(event: Any): ResultBuilder[S]
  def ++(that: Iterable[Any]): ResultBuilder[S]
  def asResult: Result
  def reject(cause: Throwable = null): ResultBuilder[S]
  def reject(message: String): ResultBuilder[S] = reject(new ResultRejectedException(message))
  def conflict: ResultBuilder[S]
}

final class EventsResultBuilder[S] (
  protected val context: CommandContext[S],
  val state: S,
  private val seqNr: Long,
  val events: immutable.IndexedSeq[Any] = Vector.empty
) extends ResultBuilder[S] {
  import CommandResult._

  private def applyEvent(e: Any) =
    new EventsResultBuilder[S](context, context.applyEvent(e, state, seqNr), seqNr+1, events :+ e)

  override def |>(f: (S) => Any): ResultBuilder[S] = this.+(f(state))

  override def +(event: Any): ResultBuilder[S] = event match {
    case None => this
    case Some(e) => applyEvent(e)
    case r: Result => new CompletedResultBuilder(context, r)
    case e: Any => applyEvent(e)
  }

  override def ++(that: Iterable[Any]): ResultBuilder[S] = {
    that.foldLeft(this: ResultBuilder[S]){ (r, e) => r + e }
  }

  override def reject(cause: Throwable = null): ResultBuilder[S] =
    new CompletedResultBuilder(context, Rejected(cause))

  override def asResult: Result =   events.headOption match {
    case None => Root
    case _ => Success(state, events)
  }

  override def conflict: ResultBuilder[S] =
    new CompletedResultBuilder(context, Conflict)
}

final class CompletedResultBuilder[S](
  protected val context: CommandContext[S],
  val result: CommandResult.Result)
extends ResultBuilder[S] {
  import CommandResult._

  override def state: S = throw new NotImplementedError("CompletedResultBuilder.state")
  override def |>(f: S => Any): ResultBuilder[S] = this
  override def +(event: Any): ResultBuilder[S] = this
  override def ++(that: Iterable[Any]): ResultBuilder[S] = this
  override def asResult: Result = result

  override def events: immutable.IndexedSeq[Any] = result match {
    case s: Success[S] => s.events
    case _ => immutable.IndexedSeq()
  }

  override def reject(cause: Throwable = null): ResultBuilder[S] = result match {
    case rejected: Rejected => this
    case Conflict => this
    case _ => new CompletedResultBuilder(context, Rejected(cause))
  }

  override def conflict: ResultBuilder[S] = result match {
    case rejected: Rejected => this
    case Conflict => this
    case _ => new CompletedResultBuilder(context, Conflict)
  }
}
