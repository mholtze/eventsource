package pimm.eventsource.aggregate

import akka.actor._
import com.typesafe.config.Config
import scala.collection.immutable
import scala.collection.mutable

trait AggregateRouterMessage {
  def aggregateId: String
  def message: Any
  def props: Props
}

object AggregateRouter {
  def props(aggregateMap: AggregateMap): Props = {
    Props(classOf[AggregateRouter], aggregateMap)
  }

  final case class ShutdownAggregate(aggregateId: String, root: Option[ActorRef] = None)

  case class StashedPendingAggregateMessage(message: AggregateRouterMessage, sender: ActorRef)
}

/** Routes messages to the correct aggregate actor based on aggregate ID.
  *
  * It may be useful to override [[createProps]] for more sophisticated aggregate actor supervision.
  * In many applications it would be useful to wrap the aggregate with a [[akka.pattern.BackoffSupervisor]] to
  * prevent thundering herds when there are database issues.
  *
  * @param aggregateMapFunction function that provides an [[AggregateMap]] cache used to track allocated
  *                             aggregate actors.
  */
class AggregateRouter(aggregateMapFunction: ActorRef => AggregateMap) extends Actor {
  import AggregateRouter._

  val aggregateMap = aggregateMapFunction(self)
  var reverseLookup = mutable.HashMap.empty[ActorRef, String]
  var shuttingDown = mutable.HashMap.empty[String, immutable.Queue[StashedPendingAggregateMessage]]

  override def receive: Receive = {
    case m: AggregateRouterMessage if shuttingDown.contains(m.aggregateId) =>
      // in shutdown, so add to pending list
      shuttingDown += m.aggregateId -> (shuttingDown(m.aggregateId) :+ StashedPendingAggregateMessage(m, sender()))
      ()
    case m: AggregateRouterMessage =>
      val root = aggregateMap(m.aggregateId).getOrElse(addActorForMessage(m))
      root.forward(m.message)

    case ShutdownAggregate(aggregateId, root) => shutdownAggregate(aggregateId, root)
    case Terminated(root) => aggregateTerminated(root)
  }

  /** Creates the [[Props]] for aggregate actor.
    *
    * @param props the [[Props]] used to create aggregate actor
    * @param aggregateId the ID of the aggregate root
    * @return the [[Props]] used to create the [[ActorRef]]
    */
  def createProps(props: Props, aggregateId: String): Props = props

  final def addActorForMessage(msg: AggregateRouterMessage): ActorRef = {
    val props = createProps(msg.props, msg.aggregateId)
    val root = context.actorOf(props, msg.aggregateId)
    context.watch(root)
    aggregateMap += msg.aggregateId -> root
    reverseLookup += root -> msg.aggregateId
    root
  }

  final def shutdownAggregate(aggregateId: String, root: Option[ActorRef]): Unit = {
    shuttingDown += aggregateId -> immutable.Queue.empty
    root match {
      case Some(actor) => actor ! AggregateActor.Shutdown
      case None => aggregateMap(aggregateId).foreach(_ ! AggregateActor.Shutdown)
    }
  }

  /** After an aggregate actor has terminated, re-send any messages stashed during shutdown so the
    * aggregate can be restarted and the messages processed.
    */
  final def aggregateTerminated(root: ActorRef): Unit = {
    val aggregateId = reverseLookup(root)
    for {
      stashed <- shuttingDown.get(aggregateId)
      msg <- stashed
    } self.tell(msg.message, msg.sender)

    aggregateMap -= aggregateId // always remove in case of unexpected shutdown
    reverseLookup -= root
    shuttingDown -= aggregateId
    ()
  }

}


class AggregateRouterFactory(actorSystem: ActorSystem, systemConfig: Config) {
  val config = systemConfig.getConfig("pimm.eventsource.aggregate.router")

  def createProps(): Props = {
    createPropsWithMap(createMap)
  }

  def createPropsWithMap(map: ActorRef => AggregateMap): Props = {
    Props(routerType, map)
  }

  def routerType: Class[_] = {
    val className = config.getString("class")
    return Class.forName(className)
  }

  def createMap: ActorRef => AggregateMap = { router =>
    AggregateMap.fromConfig(router, actorSystem, systemConfig)
  }
}
