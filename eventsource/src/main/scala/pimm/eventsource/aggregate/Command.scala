package pimm.eventsource.aggregate

import scala.collection.immutable

abstract class CommandException(message: String = null, cause: Throwable = null)
  extends AggregateException(message, cause)
{
  def reply: CommandReply.Reply[_]
}

class CommandFailureException(val reply: CommandReply.Failure[_], message: String = null, cause: Throwable = null)
  extends CommandException(message, cause)

class CommandRejectedException(val reply: CommandReply.Rejected[_], message: String = null, cause: Throwable = null)
  extends CommandException(message, cause)

class CommandConflictException(val reply: CommandReply.Conflict[_], message: String = null, cause: Throwable = null)
  extends CommandException(message, cause)

final class CommandContext[S](
  val state: S,
  private val firstSeqNr: Long,
  val applyEvent: (Any, S, Long) => S
) {
  def resultBuilder: ResultBuilder[S] = new EventsResultBuilder(this, state, firstSeqNr)
}

object CommandResult {
  sealed trait Result {
    def toReply[R](root: R): CommandReply.Reply[R]
  }

  final case object Root extends Result {
    override def toReply[R](root: R) = CommandReply.RootRef[R](root)
  }

  final case class Success[S](state: S, events: immutable.IndexedSeq[Any] = immutable.IndexedSeq.empty[Any]) extends Result {
    override def toReply[R](root: R) = CommandReply.Success[R](root, events)
  }

  final case class Failure(cause: Throwable) extends Result {
    override def toReply[R](root: R) = CommandReply.Failure[R](root, cause)
  }

  final case class Rejected(cause: Throwable) extends Result {
    override def toReply[R](root: R) = CommandReply.Rejected[R](root, cause)
  }

  final case object Conflict extends Result {
    override def toReply[R](root: R) = CommandReply.Conflict[R](root)
  }

  /** Used with optimistic concurrency to return a version conflict to the sender. */
}

object CommandRequest {
  sealed trait Request { def command: Any }
  sealed trait RequestReply extends Request
  final case class Reply(command: Any) extends RequestReply
  final case class ReplyUnboxed(command: Any) extends RequestReply
  final case class ReplyOption(command: Any) extends RequestReply
  final case class ReplyConsistent(command: Any) extends RequestReply
  final case class NoReply(command: Any) extends Request
}

object CommandReply {
  sealed trait Reply[R] {
    def root: R
  }
  sealed trait Successful[R] extends Reply[R]
  sealed trait Unsuccessful[R] extends Reply[R]

  final case class RootRef[R](root: R) extends Successful[R]
  final case class Success[R](root: R, events: immutable.IndexedSeq[Any]) extends Successful[R]
  final case class Failure[R](root: R, cause: Throwable) extends Unsuccessful[R]
  final case class Rejected[R](root: R, cause: Throwable) extends Unsuccessful[R]

  /** Used with optimistic concurrency to return a version conflict to the sender. */
  final case class Conflict[R](root: R) extends Unsuccessful[R]
}

object StandardCommands {
  final case object Root
}