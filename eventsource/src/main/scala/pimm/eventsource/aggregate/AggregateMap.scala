package pimm.eventsource.aggregate

import akka.actor.{ActorSystem, ActorRef}
import com.typesafe.config.Config

trait AggregateMap {
  def apply(persistenceId: String): Option[ActorRef]
  def +=(kv: (String, ActorRef)): Unit
  def -=(key: String): Unit
}

trait AggregateMapFromConfig {
  def fromConfig(router: ActorRef, actorSystem: ActorSystem, systemConfig: Config): AggregateMap
}

object AggregateMap {
  def fromConfig(router: ActorRef, actorSystem: ActorSystem, systemConfig: Config): AggregateMap = {
    val config = systemConfig.getConfig("pimm.eventsource.aggregate.router")
    val className = config.getString("map-class")
    val companionName = className + "$"
    val companionClass = Class.forName(companionName)
    val moduleField = companionClass.getField("MODULE$")
    val module = moduleField.get(null).asInstanceOf[AggregateMapFromConfig]

    module.fromConfig(router, actorSystem, systemConfig)
  }
}