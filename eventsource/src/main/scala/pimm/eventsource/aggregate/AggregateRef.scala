package pimm.eventsource.aggregate

import akka.actor._
import akka.contrib.eventsource.pimm.ProxyActorRef
import akka.pattern.AskableActorRef
import akka.util.Timeout

import scala.concurrent.Future
import scala.reflect.ClassTag

/** [[Props]] factory for aggregate actor. Normally extends the companion object of an aggregate actor.
  *
  * {{{
  *   // Example
  *
  *   // MyAggregate and companion object which extends AggregateProps
  *   class MyAggregate(val aggregateId: String) extends AggregateActor[MyRoot] { }
  *
  *   object MyAggregate extends AggregateProps[MyRoot] {
  *     override def props(aggregateId: String): Props(new MyAggregate(aggregateId))
  *   }
  *
  *   // Create AggregateRef and send message
  *   EventSourceSystem(actorSystem).to(MyAggregate, "123") ! "my message"
  * }}}
  */
trait AggregateProps[R] {
  def props(aggregateId: String): Props
}


/** A wrapper around the [[AggregateRouter]] ActorRef that adds functionality to simplify sending
  * messages to aggregate roots.
  */
abstract class AggregateRef[R : ClassTag] extends ProxyActorRef {
  import CommandRequest._

  /** The aggregate router reference. */
  def router: ActorRef

  /** Factory that creates the appropriate [[AggregateRouterMessage]] for wrapper. */
  def createRouterMessage(message: Any): AggregateRouterMessage

  final override def !(message: Any)(implicit sender: ActorRef = Actor.noSender): Unit =
    router ! createRouterMessage(message)

  override def path: ActorPath = router.path

  final def ask(message: Any)(implicit timeout: Timeout, sender: ActorRef = Actor.noSender): Future[Any] =
    message match {
      case _: Request => internalAsk(createRouterMessage(message), timeout, sender)
      case _ => internalAsk(createRouterMessage(Reply(message)), timeout, sender)
    }

  final def ?(message: Any)(implicit timeout: Timeout, sender: ActorRef = Actor.noSender): Future[Any] =
    message match {
      case _: Request => internalAsk(createRouterMessage(message), timeout, sender)
      case _ => internalAsk(createRouterMessage(Reply(message)), timeout, sender)
    }

  final def root(implicit timeout: Timeout, sender: ActorRef = Actor.noSender): Future[R] =
    internalAsk(createRouterMessage(ReplyUnboxed(StandardCommands.Root)), timeout, sender).mapTo[R]

  final def rootOption(implicit timeout: Timeout, sender: ActorRef = Actor.noSender): Future[Option[R]] = {
    internalAsk(createRouterMessage(ReplyOption(StandardCommands.Root)), timeout, sender).mapTo[Option[R]]
  }

  /** Convenience method to ask and return the root as an unboxed value. */
  final def askUnboxed(message: Any)(implicit timeout: Timeout, sender: ActorRef = Actor.noSender): Future[R] = {
    internalAsk(createRouterMessage(ReplyUnboxed(message)), timeout, sender).mapTo[R]
  }

  /** Convenience method to ask and return the root as an Option. */
  final def askOption(message: Any)(implicit timeout: Timeout, sender: ActorRef = Actor.noSender): Future[Option[R]] = {
    internalAsk(createRouterMessage(ReplyOption(message)), timeout, sender).mapTo[Option[R]]
  }

  /** Convenience method to ask and reply after eventually consistent acknowledgement. */
  final def askConsistent(message: Any)(implicit timeout: Timeout, sender: ActorRef = Actor.noSender): Future[Any] = {
    internalAsk(createRouterMessage(ReplyConsistent(message)), timeout, sender)
  }

  private final def internalAsk(message: Any, timeout: Timeout, sender: ActorRef): Future[Any] ={
    new AskableActorRef(router).ask(message)(timeout, sender)
  }
}

/** Factory to create [[AggregateRef]] actors to simplify sending messages to aggregate actors. */
trait AggregateRefProvider {
  def aggregateRouter: ActorRef
  protected def actorRefProvider: ActorRefProvider

  /** Create an [[AggregateRef]] for the [[AggregateActor]] class and aggregateId.
    *
    * {{{
    *   // Example
    *
    *   // MyAggregate and companion object which extends AggregateProps
    *   class MyAggregate(val aggregateId: String) extends AggregateActor[MyRoot] { }
    *
    *   object MyAggregate extends AggregateProps[MyRoot] {
    *     override def props(aggregateId: String): Props(new MyAggregate(aggregateId))
    *   }
    *
    *   val eventSystem = EventSourceSystem(actorSystem)
    *
    *   // Create AggregateRef and send message
    *   eventSystem.to[MyRoot](classOf[MyAggregate], "123") ! "my message"
    * }}}
    */
  final def to[R : ClassTag](actorClass: Class[_], aggregateId: String): AggregateRef[R] = new AggregateRef[R] {
    final override def router = aggregateRouter
    final override def provider = actorRefProvider
    final override def createRouterMessage(message: Any) =
      AggregatePropsMessage(Props(actorClass, aggregateId), aggregateId, message)
  }

  /** Create an [[AggregateRef]] for the [[AggregateActor]] props and aggregateId.
    *
    * {{{
    *   // Example
    *
    *   // MyAggregate and companion object which extends AggregateProps
    *   class MyAggregate(val aggregateId: String) extends AggregateActor[MyRoot] { }
    *
    *   object MyAggregate extends AggregateProps[MyRoot] {
    *     override def props(aggregateId: String): Props(new MyAggregate(aggregateId))
    *   }
    *
    *   val eventSystem = EventSourceSystem(actorSystem)
    *
    *   // Create AggregateRef and send message
    *   eventSystem.to(MyAggregate, "123") ! "my message"
    * }}}
    */
  final def to[R : ClassTag](props: AggregateProps[R], aggregateId: String): AggregateRef[R] = new AggregateRef[R] {
    final override def router = aggregateRouter
    final override def provider = actorRefProvider
    final override def createRouterMessage(message: Any) =
      AggregatePropsMessage(props.props(aggregateId), aggregateId, message)
  }

  /** Create an [[AggregateRef]] for the [[AggregateActor]] type and aggregateId.
    *
    * {{{
    *   // Example
    *
    *   class MyAggregate(val aggregateId: String) extends AggregateActor[MyRoot] { }
    *
    *   val eventSystem = EventSourceSystem(actorSystem)
    *
    *   // Create AggregateRef and send message
    *   eventSystem.to[MyAggregate, String]("123") ! "my message"
    * }}}
    */
  final def to[T <: Actor: ClassTag, R: ClassTag](aggregateId: String): AggregateRef[R] = new AggregateRef[R] {
    final override def router = aggregateRouter
    final override def provider = actorRefProvider
    final override def createRouterMessage(message: Any) =
      AggregateMessage[T](aggregateId, message)
  }

  /** Syntactical sugar to create an [[AggregateRef]] for the [[AggregateActor]] props and aggregateId.
    *
    * {{{
    *   // instead of
    *   eventSystem.to(MyAggregate, "123") ! "my message"
    *   // you can do
    *   eventSystem |> (MyAggregate, "123") ! "my message"
    * }}}
    */
  final def |>[R : ClassTag](props: AggregateProps[R], aggregateId: String): AggregateRef[R] = to(props, aggregateId)

  /** Syntactical sugar to create an [[AggregateRef]] for the [[AggregateActor]] type and aggregateId.
    *
    * {{{
    *   // instead of
    *   eventSystem.to[MyAggregate, String]("123") ! "my message"
    *   // you can do
    *   eventSystem |> [MyAggregate, String]("123") ! "my message"
    * }}}
    */
  final def |>[T <: Actor : ClassTag, R: ClassTag](aggregateId: String): AggregateRef[R] = to[T, R](aggregateId)
}