package pimm.eventsource.aggregate

import akka.actor.{Actor, Props}

import scala.reflect.ClassTag

case class AggregateMessage[T <: Actor: ClassTag](
  aggregateId: String,
  message: Any
) extends AggregateRouterMessage {
  override def props: Props = Props(implicitly[ClassTag[T]].runtimeClass, aggregateId)
}

case class AggregatePropsMessage(
  props: Props,
  aggregateId: String,
  message: Any
) extends AggregateRouterMessage