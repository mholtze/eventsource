package pimm.eventsource.aggregate

//import scala.reflect.ClassTag
//import scala.collection.mutable
//
///** Trait required by events that implement there own apply functionality. */
//trait Applyable[R] {
//  def apply(root: R): R
//}
//
//object Applyable {
//  type Apply[R] = PartialFunction[Any, R]
//
//  /** Attempts apply events by match against a [[PartialFunction]] or applying by the event as an [[Applyable]]. */
//  def apply[R](applyEvent: Apply[R])(root: R, event: Any): R = {
//    applyEvent.orElse[Any, R] {
//      case applyable: Applyable[R] => applyable.apply(root)
//    } (event)
//  }
//}
//
///** Map events to apply handlers. This is to facilitate handling events that are generated code and
//  * cannot directly implement Applyable.
//  */
//trait Applyables[R] {
//  def matches(event: Any): Boolean
//  def apply(root: R, event: Any): R
//}
//
///** Base trait for wrapper that handler externalized apply for events. */
//trait ApplyInvoker[R] {
//  def apply(root: R, event: Any): R
//}
//
///** [[ApplyInvoker]] that creates uses a factory function to create an [[Applyable]] wrapper for the event. */
//class ApplyableFactory[E, R](private val f: (E) => Applyable[R]) extends ApplyInvoker[R] {
//  override def apply(root: R, event: Any): R = {
//    val typedEvent = event.asInstanceOf[E]
//    val applyable = f(typedEvent)
//    applyable(root)
//  }
//}
//
///** [[ApplyInvoker]] that uses a function to handle apply. */
//class ApplyFunction[E, R](private val f: (R, E) => R) extends ApplyInvoker[R] {
//  override def apply(root: R, event: Any): R = {
//    val typedEvent = event.asInstanceOf[E]
//    f(root, typedEvent)
//  }
//}
//
//
///** [[Applyables]] implementation that maps events to [[ApplyInvoker]] by event class.
//  *
//  * {{{
//  *   // Example of building an applyables map.
//  *   object StringApplyables {
//  *     case class ApplyAppend(event: Append) extends Applyable[String] {
//  *       override def apply(root: String): String = root + event.value
//  *     }
//  *
//  *     val applyables = ApplyablesMap[String]{ classOf =>
//  *       // Applyable wrapper
//  *       classOf[Append] -> ApplyAppend
//  *       // Apply function
//  *       classOf[AppendTwice] -> { (r, e) => r + e.value + e.value }
//  *     }
//  *   }
//  * }}}
//  */
//class ApplyablesMap[R](
//  private val invokers: Map[Class[_], ApplyInvoker[R]]
//) extends Applyables[R] {
//
//  override def matches(event: Any): Boolean = invokers.contains(event.getClass)
//
//  override def apply(root: R, event: Any): R = {
//    val invoker = invokers(event.getClass)
//    invoker.apply(root, event)
//  }
//}
//
//
//object ApplyablesMap {
//  class ClassOfBuilder[R] {
//    private[aggregate] val invokers = mutable.HashMap.empty[Class[_], ApplyInvoker[R]]
//
//    def apply[E: ClassTag](f: (E) => Applyable[R]): Unit = apply[E] -> f
//    def apply[E: ClassTag](f: (R, E) => R): Unit = apply[E] -> f
//    def apply[E: ClassTag]: ClassOfCase[R, E] = new ClassOfCase(invokers, implicitly[ClassTag[E]].runtimeClass)
//  }
//
//  class ClassOfCase[R, E](
//    private val invokers: mutable.Map[Class[_], ApplyInvoker[R]],
//    private val clazz: Class[_]) {
//
//    def ->(f: (E) => Applyable[R]): Unit = {
//      val invoker = new ApplyableFactory(f)
//      invokers += clazz -> invoker
//      ()
//    }
//
//    def ->(f: (R, E) => R): Unit = {
//      val invoker = new ApplyFunction(f)
//      invokers += clazz -> invoker
//      ()
//    }
//  }
//
//  def apply[R](b: (ClassOfBuilder[R]) => Unit): Applyables[R] = {
//    val builder = new ClassOfBuilder[R]
//    b(builder)
//    new ApplyablesMap[R](builder.invokers.toMap)
//  }
//}
