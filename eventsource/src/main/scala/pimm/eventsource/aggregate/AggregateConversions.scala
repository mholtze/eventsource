package pimm.eventsource.aggregate

object AggregateConversions {
  implicit def EventsResultToCommandResult[S](results : ResultBuilder[S]) =
    results.asResult
}
