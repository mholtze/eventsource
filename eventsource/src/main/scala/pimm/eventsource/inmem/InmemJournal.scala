package pimm.eventsource.inmem

import akka.persistence.query.EventEnvelope
import akka.persistence.{PersistentRepr, AtomicWrite}
import akka.persistence.journal.AsyncWriteJournal
import pimm.eventsource.events.PersistentEventEnvelope

import scala.collection.immutable
import scala.concurrent.Future
import scala.util.Try


final case class InmemEventEnvelope(
  offset: Long,
  persistenceId: String,
  sequenceNr: Long,
  globalSequenceNr: Long,
  event: Any
) extends PersistentEventEnvelope {

  override def aggregateId: String = persistenceId

  /** Convert to a regular event envelope that does not have a global sequence number. */
  lazy val toEventEnvelope: EventEnvelope = EventEnvelope(offset, persistenceId, sequenceNr, event)
}

private[eventsource] final case class GlobalPersistentRepr(repr: PersistentRepr, globalSeqNr: Long) {
  def toGlobalEnvelope(offset: Long) = InmemEventEnvelope(
    offset, repr.persistenceId, repr.sequenceNr, globalSeqNr, repr.payload
  )
  def toEventEnvelope(offset: Long) = EventEnvelope(
    offset, repr.persistenceId, repr.sequenceNr, repr.payload
  )
}

/**
  * In-memory journal for testing purposes only. This is based on the inmem journal bundling with Akka but with
  * support for global event ordering.
  */
private[eventsource] class InmemJournal extends AsyncWriteJournal {
  private val inmemStore = InmemStoreExtension(context.system).inmemStore

  override def asyncWriteMessages(messages: immutable.Seq[AtomicWrite]): Future[immutable.Seq[Try[Unit]]] = {
    try {
      for (w ← messages; p ← w.payload) {
        inmemStore.addEvent(p)
      }
      Future.successful(Nil) // all good
    } catch {
      case t: Throwable => Future.failed(t)
    }
  }

  override def asyncReadHighestSequenceNr(persistenceId: String, fromSequenceNr: Long): Future[Long] = {
    Future.successful(inmemStore.highestEventSequenceNr(persistenceId))
  }

  override def asyncReplayMessages(persistenceId: String, fromSequenceNr: Long, toSequenceNr: Long, max: Long)(
    recoveryCallback: PersistentRepr ⇒ Unit): Future[Unit] = {
    val highest = inmemStore.highestEventSequenceNr(persistenceId)
    if (highest != 0L && max != 0L)
      inmemStore.readEvents(persistenceId, fromSequenceNr, math.min(toSequenceNr, highest), max).foreach(recoveryCallback)
    Future.successful(())
  }

  def asyncDeleteMessagesTo(persistenceId: String, toSequenceNr: Long): Future[Unit] = {
    val toSeqNr = math.min(toSequenceNr, inmemStore.highestEventSequenceNr(persistenceId))
    var snr = 1L
    while (snr <= toSeqNr) {
      inmemStore.deleteEvent(persistenceId, snr)
      snr += 1
    }
    Future.successful(())
  }
}