package pimm.eventsource.inmem

import akka.actor.{ActorRef, Stash, Props, ExtendedActorSystem}
import akka.persistence.query._
import akka.persistence.query.scaladsl.{CurrentEventsByPersistenceIdQuery, CurrentPersistenceIdsQuery}
import akka.persistence.query.ReadJournalProvider
import akka.stream.ActorMaterializer
import akka.stream.actor.ActorPublisher
import akka.stream.scaladsl.{Sink, Source}
import com.typesafe.config.Config
import org.reactivestreams.{Subscription, Subscriber}

object InmemReadJournal {
  val Identifier = "pimm.eventsource.inmem.readjournal"
}

class InmemReadJournal(system: ExtendedActorSystem, config: Config) extends ReadJournalProvider {
  private val inmemStore = InmemStoreExtension(system).inmemStore

  override def scaladslReadJournal(): scaladsl.ReadJournal = new ScalaDslInmemReadJournal(inmemStore)
  override def javadslReadJournal(): javadsl.ReadJournal = new JavaDslInmemReadJournal(inmemStore)
}

class ScalaDslInmemReadJournal(inmemStore: InmemStore) extends scaladsl.ReadJournal
  with CurrentPersistenceIdsQuery with CurrentEventsByPersistenceIdQuery
{
  def allEvents(): Source[EventEnvelope,Unit] =
    Source(inmemStore
      .eventsOrderedGlobally.toStream
      .zipWithIndex
      .map(x => x._1.toEventEnvelope(x._2.toLong)))

  override def currentPersistenceIds(): Source[String, Unit] =
    Source(inmemStore.events.keys.toList)

  override def currentEventsByPersistenceId(persistenceId: String, fromSequenceNr: Long, toSequenceNr: Long): Source[EventEnvelope, Unit] = {
    require(persistenceId != null, "PersistenceId must not be null")
    inmemStore.events.get(persistenceId) match {
      case None =>
        Source(List.empty)
      case Some(globalReprs) =>
        Source(globalReprs.toStream
          .filter(x => x.repr.sequenceNr >= fromSequenceNr && x.repr.sequenceNr <= toSequenceNr)
          .zipWithIndex
          .map(x => x._1.toEventEnvelope(x._2.toLong)))
    }
  }

  def allEventsInGlobalOrder(globalFrom: Long, globalTo: Long): Source[InmemEventEnvelope, Unit] =
    Source(inmemStore
      .eventsOrderedGlobally.toStream
      .filter(x => x.globalSeqNr >= globalFrom && x.globalSeqNr <= globalTo)
      .zipWithIndex
      .map(x => x._1.toGlobalEnvelope(x._2.toLong)))

  def liveGlobalEvents(globalFrom: Option[Long] = Option.empty): Source[InmemEventEnvelope, Unit] =
    Source.actorPublisher[InmemEventEnvelope](InmemGlobalEventsLiveQuery.props(globalFrom))
      .mapMaterializedValue(_ => ())
}


class JavaDslInmemReadJournal(inmemStore: InmemStore) extends javadsl.ReadJournal {

}

class InmemGlobalEventsLiveQuery(globalFrom: Option[Long]) extends ActorPublisher[InmemEventEnvelope] with Stash {
  import akka.stream.actor.ActorPublisherMessage._
  import context._
  import InmemGlobalEventsLiveQuery._

  private implicit val materializer = ActorMaterializer()(context.system)
  private var lastGlobalSequenceNr: Long = 0L
  private var buf = Vector.empty[InmemEventEnvelope]
  private var allEventsSubscription: Option[Subscription] = Option.empty
  private var transitioningToLive: Boolean = false

  @throws[Exception](classOf[Exception])
  override def preStart(): Unit = {
    super.preStart()
    system.eventStream.subscribe(self, classOf[InmemEventEnvelope])
    self ! globalFrom
  }

  @throws[Exception](classOf[Exception])
  override def postStop(): Unit = {
    system.eventStream.unsubscribe(self)
    super.postStop()
  }

  override def receive: Receive = {
    case SubscriptionTimeoutExceeded =>
      context.stop(self)
    case Some(from: Long) =>
      // the -1 is needed since lastGlobalSequenceNr is the last processed number
      // and catchup is from lastGlobalSequenceNr + 1
      lastGlobalSequenceNr = from - 1
      startCatchup(transitionToLive = false)
    case None =>
      startLive()
  }

  private def catchup: Receive = {
    case AllEventsSubscribe(sub) =>
      allEventsSubscription = Option.apply(sub)
      requestCatchup()
    case AllEventsError(cause) =>
      stopCatchup(false)
      onError(cause)
    case AllEventsComplete =>
      stopCatchup(false)
      if (transitioningToLive)
        startLive()
      else
        startCatchup(transitionToLive = true)
    case AllEventsNext(env: InmemEventEnvelope) =>
      addToBuffer(env)
      requestCatchup()
    case Request(_) =>
      deliverBuf()
    case Cancel =>
      stopCatchup(true)
      context.stop(self)
    case InmemEventEnvelope if transitioningToLive =>
      stash()
  }

  private def startCatchup(transitionToLive: Boolean): Unit = {
    require(allEventsSubscription.isEmpty, "All events subscription already exists")

    val readJournal = PersistenceQuery(context.system)
      .readJournalFor[ScalaDslInmemReadJournal](InmemReadJournal.Identifier)

    val source = readJournal.allEventsInGlobalOrder(lastGlobalSequenceNr + 1, Long.MaxValue)
    source.runWith(Sink(new AllEventsSubscriber(self)))

    transitioningToLive = transitionToLive
    become(catchup)
  }

  private def stopCatchup(cancel: Boolean): Unit = {
    if (cancel)
      allEventsSubscription.foreach(_.cancel())
    allEventsSubscription = Option.empty
  }

  private def requestCatchup(): Unit = {
    allEventsSubscription.foreach(_.request(10L))
  }

  private def live: Receive = {
    // under certain circumstances, like testing, live events can arrive after catchup for the same event
    case event: InmemEventEnvelope if (event.globalSequenceNr > lastGlobalSequenceNr) => addToBuffer(event)
    case Request(_) => deliverBuf()
    case Cancel => context.stop(self)
  }

  private def startLive(): Unit = {
    transitioningToLive = false
    unstashAll()
    become(live)
  }

  private def addToBuffer(event: InmemEventEnvelope): Unit = {
    buf :+= event
    deliverBuf()
  }

  final def deliverBuf(): Unit =
    if (totalDemand > 0) {
      val (use, keep) = buf.splitAt(totalDemand.toInt)
      if (!use.isEmpty) {
        buf = keep
        use foreach onNext
        lastGlobalSequenceNr = use.last.globalSequenceNr
      }
    }
}

object InmemGlobalEventsLiveQuery {
  def props(globalFrom: Option[Long] = Option.empty): Props = Props(new InmemGlobalEventsLiveQuery(globalFrom))

  private[eventsource] sealed trait AllEventsSubscriberMessage {}
  private[eventsource] final case class AllEventsNext(element: Any) extends AllEventsSubscriberMessage
  private[eventsource] final case class AllEventsError(cause: Throwable) extends AllEventsSubscriberMessage
  private[eventsource] final case class AllEventsSubscribe(subscription: Subscription) extends AllEventsSubscriberMessage
  private[eventsource] final case object AllEventsComplete extends AllEventsSubscriberMessage
}

private[eventsource] final class AllEventsSubscriber(val impl: ActorRef) extends Subscriber[InmemEventEnvelope] {
  import InmemGlobalEventsLiveQuery._

  override def onError(cause: Throwable): Unit = {
    impl ! AllEventsError(cause)
  }
  override def onComplete(): Unit = impl ! AllEventsComplete
  override def onNext(element: InmemEventEnvelope): Unit = {
    impl ! AllEventsNext(element)
  }
  override def onSubscribe(subscription: Subscription): Unit = {
    impl ! AllEventsSubscribe(subscription)
  }
}