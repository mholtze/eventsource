package pimm.eventsource.inmem

import akka.actor._
import akka.persistence.{SelectedSnapshot, PersistentRepr, SnapshotMetadata}

import scala.collection.immutable

private[eventsource] object InmemStoreExtension extends ExtensionId[InmemStoreExtension] with ExtensionIdProvider {
  override def lookup = InmemStoreExtension
  override def createExtension(system: ExtendedActorSystem) = new InmemStoreExtension(system)
  /** Java API: retrieve the Count extension for the given system. */
  override def get(system: ActorSystem): InmemStoreExtension = super.get(system)
}

private[eventsource] class InmemStoreExtension(system: ActorSystem) extends Extension {
  val inmemStore = new InmemStore(system)
}

/**
 * Simple in-memory storage container for events and snapshots that supports globally ordering.
 *
 * This implementation is intended for testing only. Multithreading is handled by basic concurrency with
  * "synchronized" and immutable storage structures.
 */
private[eventsource] final class InmemStore(system: ActorSystem) {
  private var lastGlobalSequenceNr = 0L
  private var _eventsOrderedGlobally = Vector.empty[GlobalPersistentRepr]
  private var _events = Map.empty[String, Vector[GlobalPersistentRepr]]
  private var _snapshots = Map.empty[SnapshotMetadata, Any]
  private var _eventsEnabled = true
  private var _snapshotsEnabled = true

  def eventsOrderedGlobally = synchronized { _eventsOrderedGlobally }
  def events = synchronized { _events }
  def snapshots = synchronized { _snapshots }

  def eventsEnabled = synchronized { _eventsEnabled }
  def eventsEnabled_=(enabled:Boolean): Unit = synchronized { _eventsEnabled = enabled }

  def snapshotsEnabled = synchronized { _snapshotsEnabled }
  def snapshotsEnabled_=(enabled:Boolean): Unit = synchronized { _snapshotsEnabled = enabled }

  def clear(): Unit = synchronized {
    lastGlobalSequenceNr = 0L
    _eventsOrderedGlobally = Vector.empty[GlobalPersistentRepr]
    _events = Map.empty[String, Vector[GlobalPersistentRepr]]
    _snapshots = Map.empty[SnapshotMetadata, Any]
  }

  def addEvent(p: PersistentRepr): Unit = synchronized {
    assert(_eventsEnabled, "Events disabled.")
    require(p.payload.isInstanceOf[java.io.Serializable], s"Type ${p.payload.getClass} is currently unsupported")

    lastGlobalSequenceNr = lastGlobalSequenceNr + 1
    val gp = GlobalPersistentRepr(p, lastGlobalSequenceNr)
    _eventsOrderedGlobally = _eventsOrderedGlobally :+ gp
    _events = _events + (_events.get(p.persistenceId) match {
      case Some(ms) ⇒ p.persistenceId -> (ms :+ gp)
      case None ⇒ p.persistenceId -> Vector(gp)
    })
    system.eventStream.publish(gp.toGlobalEnvelope(gp.globalSeqNr))
  }

  def deleteEvent(pid: String, snr: Long): Unit = synchronized {
    for (pidEvents <- _events.get(pid)) {
      val updated = pidEvents.filter(e => e.repr.sequenceNr != snr)
      if (updated.isEmpty)
        _events -= pid
      else
        _events += pid -> updated
    }
  }

  def readEvents(pid: String, fromSnr: Long, toSnr: Long, max: Long): immutable.Seq[PersistentRepr] = events.get(pid) match {
    case Some(ms) ⇒ ms.toStream.map(_.repr).filter(m ⇒ m.sequenceNr >= fromSnr && m.sequenceNr <= toSnr).take(safeLongToInt(max))
    case None     ⇒ Nil
  }

  def highestEventSequenceNr(pid: String): Long = {
    val snro = for {
      ms ← events.get(pid)
      m ← ms.lastOption
    } yield m.repr.sequenceNr
    snro.getOrElse(0L)
  }

  def saveSnapshot(metadata: SnapshotMetadata, snapshot: Any): Unit = synchronized {
    assert(_snapshotsEnabled, "Snapshots disabled.")
    _snapshots += metadata -> snapshot
    system.eventStream.publish(SelectedSnapshot(metadata, snapshot))
  }

  def deleteSnapshot(metadata: SnapshotMetadata): Unit = synchronized {
    val s = _snapshots.get(metadata)
    val size = _snapshots.size
    _snapshots -= metadata
    val size2 = _snapshots.size
  }

  def readSnapshots(persistenceId: String): IndexedSeq[SelectedSnapshot] = {
    val candidates = snapshots.keys.filter(_.persistenceId == persistenceId).toIndexedSeq
    for {
      metadata <- candidates.sorted(Ordering[SnapshotMetadata])
      snapshot <- snapshots.get(metadata)
    } yield SelectedSnapshot(metadata, snapshot)
  }

  private def safeLongToInt(l: Long): Int =
    if (Int.MaxValue < l) Int.MaxValue else l.toInt
}