package pimm.eventsource.inmem

import akka.persistence.{SelectedSnapshot, SnapshotSelectionCriteria, SnapshotMetadata}
import akka.persistence.snapshot.SnapshotStore

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future


/**
  * In-memory snapshot store loosely based on https://github.com/migesok/akka-persistence-in-memory-snapshot-store.
  */
class InmemSnapshotStore extends SnapshotStore {
  private val inmemStore = InmemStoreExtension(context.system).inmemStore

  override def loadAsync(persistenceId: String, criteria: SnapshotSelectionCriteria): Future[Option[SelectedSnapshot]] = Future {
    val snapshots = inmemStore.snapshots
    val candidates = snapshots.keys.filter(satisfies(persistenceId, criteria)).toList
    for {
      metadata <- candidates.sorted(Ordering[SnapshotMetadata].reverse).headOption
      snapshot <- snapshots.get(metadata)
    } yield SelectedSnapshot(metadata, snapshot)
  }

  override def saveAsync(metadata: SnapshotMetadata, snapshot: Any): Future[Unit] = Future {
    inmemStore.saveSnapshot(metadata, snapshot)
  }

  override def deleteAsync(metadata: SnapshotMetadata): Future[Unit] = Future {
    val toDelete = inmemStore.snapshots.keys.filter(m =>
      m.persistenceId == metadata.persistenceId && m.sequenceNr == metadata.sequenceNr
    )
    toDelete.foreach(inmemStore.deleteSnapshot)
  }

  override def deleteAsync(persistenceId: String, criteria: SnapshotSelectionCriteria): Future[Unit] = Future {
    val toDelete = inmemStore.snapshots.keys.filter(satisfies(persistenceId, criteria)(_))
    toDelete.foreach(inmemStore.deleteSnapshot)
  }

  private def satisfies(persistenceId: String, criteria: SnapshotSelectionCriteria)(metadata: SnapshotMetadata): Boolean =
    metadata.persistenceId == persistenceId &&
      metadata.sequenceNr <= criteria.maxSequenceNr &&
      metadata.timestamp <= criteria.maxTimestamp
}
