package pimm.eventsource.inmem

import java.util.concurrent.atomic.AtomicLong

import akka.actor.{ActorRef, ActorSystem}
import akka.persistence.query.PersistenceQuery
import akka.stream.scaladsl.Source
import pimm.eventsource.events.{EventualsApi, PersistentEventEnvelope}

import scala.concurrent.Future

class InmemEventualsApi(system: ActorSystem, eventuals: ActorRef) extends EventualsApi {
  val readJournal = PersistenceQuery(system).readJournalFor[ScalaDslInmemReadJournal](InmemReadJournal.Identifier)
  private val lastSeqNr = new AtomicLong(0L)

  override def readLastGlobalSeqNr(): Future[Long] = Future.successful(lastSeqNr.get())

  override def saveLastGlobalSeqNr(lastSeqNr: Long): Unit = this.lastSeqNr.set(lastSeqNr)

  override def allEventsLive(globalFrom: Long): Source[PersistentEventEnvelope, Unit] =
    readJournal.liveGlobalEvents(Some(globalFrom))
}
