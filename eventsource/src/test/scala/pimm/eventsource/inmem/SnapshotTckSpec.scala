package pimm.eventsource.inmem

import akka.persistence.snapshot.SnapshotStoreSpec
import com.typesafe.config.ConfigFactory

object SnapshotTckSpec {

  def config = ConfigFactory.parseString(s"""
    |akka.persistence.journal.plugin = "pimm.eventsource.inmem.journal"
    |akka.persistence.snapshot-store.plugin = "pimm.eventsource.inmem.snapshot"
    |akka.persistence.journal.leveldb.native = off
    |""".stripMargin)
}

class SnapshotTckSpec extends SnapshotStoreSpec(SnapshotTckSpec.config)  {
}
