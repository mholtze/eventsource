package pimm.eventsource.inmem

import akka.persistence.journal.JournalSpec
import com.typesafe.config.ConfigFactory

object JournalTckSpec {

  def config = ConfigFactory.parseString(s"""
     |akka.persistence.journal.plugin = "pimm.eventsource.inmem.journal"
     |""".stripMargin)
  }

class JournalTckSpec extends JournalSpec(JournalTckSpec.config) {
}
