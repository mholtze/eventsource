package pimm.eventsource.aggregate

import akka.pattern.ask
import pimm.eventsource.inmem.InmemStoreExtension
import scala.concurrent.Await
import scala.concurrent.duration._

class AggregateActorSpec extends BaseAggregateActorTest {
  import BaseAggregateActorTest._
  import CommandRequest._
  import CommandReply._

  def timeout: FiniteDuration = 100 second

  "An AggregateActor" should "provide the root on read request" in withSystem { (system, eventSystem) =>
    val msg = newMessage("agg-1", Reply(StandardCommands.Root))
    val future = eventSystem.aggregateRouter ? msg
    val result = Await.result(future, timeout)
    result should be (RootRef("r1"))
    ()
  }

  it should "persist events after executing a command" in withSystem(configWithEventProbe) { (system, eventSystem) =>
    val probe = ProbingConsistencyBuilder(system).probe
    val msg = newMessage("agg-1", NoReply(AppendCommand("r2")))
    eventSystem.aggregateRouter ! msg
    probe.expectMsg(timeout, Append("r2"))
    ()
  }

  it should "send a reply after persisting a command" in withSystem { (system, eventSystem) =>
    val msg = newMessage("agg-1", Reply(AppendCommand("r2")))
    val future = eventSystem.aggregateRouter ? msg
    val result = Await.result(future, timeout)
    result should be (Success("r1 r2", Vector(Append("r2"))))
    ()
  }

  it should "send a single reply after persisting a group of events" in withSystem(configWithEventProbe) { (system, eventSystem) =>
    val probe = ProbingConsistencyBuilder(system).probe
    val msg = newMessage("agg-1", Reply(AppendCommand(Seq("r2", "r3"))))
    val future = eventSystem.aggregateRouter ? msg

    val result = Await.result(future, timeout)
    result should be (Success("r1 r2 r3", Vector(Append("r2"), Append("r3"))))
    probe.expectMsg(timeout, Append("r2"))
    probe.expectMsg(timeout, Append("r3"))
    ()
  }

  it should "handle replies to acknowledge consistent" in withSystem(configWithEventProbe) { (system, eventSystem) =>
    val probe = ProbingConsistencyBuilder(system).probe
    val msg = newMessage("agg-1", ReplyConsistent(AppendCommand("r2")))

    val future = eventSystem.aggregateRouter ? msg
    probe.expectMsg(timeout, Append("r2"))

    val result = Await.result(future, timeout)
    result should be (Success("r1 r2", Vector(Append("r2"))))
    ()
  }

  it should "deal with exceptions from commands" in withSystem { (system, eventSystem) =>
    val msg = newMessage("agg-1", Reply(ErrorCommand))
    val future = eventSystem.aggregateRouter ? msg

    val failure = Await.result(future.failed, timeout)
    val cmdEx = failure.asInstanceOf[CommandFailureException]
    cmdEx.getMessage should be (s"Command [${ErrorCommand.getClass.getCanonicalName}] failed.")
    cmdEx.reply.root should be ("r1")
    cmdEx.reply.cause.getMessage should be ("ErrorCommand")
    ()
  }

  it should "support commands rejecting requests" in withSystem { (system, eventSystem) =>
    val msg = newMessage("agg-1", Reply(RejectCommand))
    val future = eventSystem.aggregateRouter ? msg

    val failure = Await.result(future.failed, timeout)
    val cmdEx = failure.asInstanceOf[CommandRejectedException]
    cmdEx.getMessage should be (s"Command [${RejectCommand.getClass.getCanonicalName}] rejected.")
    cmdEx.reply.root should be ("r1")
    cmdEx.reply.cause.getMessage should be ("RejectCommand")
    ()
  }

  it should "send failure replies for onPersistFailure" in withSystem { (system, eventSystem) =>
    val inmemStore = InmemStoreExtension(system).inmemStore
    inmemStore.eventsEnabled = false

    val cmd = AppendCommand("r2")
    val msg = newMessage("agg-1", Reply(cmd))
    val future = eventSystem.aggregateRouter ? msg

    val failure = Await.result(future.failed, timeout)
    val cmdEx = failure.asInstanceOf[CommandFailureException]
    cmdEx.getMessage should startWith (s"Command [${cmd.getClass.getCanonicalName}] failed. Failed to persist event type [${classOf[Append].getCanonicalName}]")
    cmdEx.reply.root should be ("r1")
    ()
  }
}
