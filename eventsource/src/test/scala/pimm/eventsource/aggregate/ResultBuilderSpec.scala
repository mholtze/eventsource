package pimm.eventsource.aggregate

import pimm.eventsource.BaseActorTest
import pimm.eventsource.aggregate.AggregateConversions._

class ResultBuilderSpec extends BaseActorTest {
  import CommandResult._

  case class Append(value: String)

  def createContext(state: String) = new CommandContext[String](state, 1L, (event, s, n) => event match {
    case Append(value) => s + " " + value
  })

  "A ResultBuilder" should "return itself if None is added as an event" in {
    val context = createContext("r1")
    val results1 = context.resultBuilder
    results1 + None should be theSameInstanceAs(results1)
  }

  it should "apply and append an event if Some is added as an event" in {
    val context = createContext("r1")
    val event = Append("r2")
    val results1 = context.resultBuilder
    val results2 = results1 + Some(event)
    results2.state should be ("r1 r2")
    results2.events should be (Vector(event))
  }

  it should "apply and append a directly added event" in {
    val context = createContext("r1")
    val event = Append("r2")
    val results1 = context.resultBuilder
    val results2 = results1 + event
    results2.state should be ("r1 r2")
    results2.events should be (Vector(event))
  }

  it should "create a RootValue result if there are no events" in {
    val context = createContext("r1")
    val results1 = context.resultBuilder
    val theResult = results1.asResult
    theResult should be (Root)
  }

  it should "create a Success result if there are events" in {
    val context = createContext("r1")
    val event = Append("r2")
    val results1 = context.resultBuilder
    val results2 = results1 + event
    val theResult = results2.asResult
    theResult should be (Success(results2.state, Vector(event)))
  }

  it should "apply and append a mixed list of events" in {
    val context = createContext("r1")
    val events = Seq(Append("r2"), None, Some(Append("r4")))
    val results1 = context.resultBuilder
    val results2 = results1 ++ events
    results2.state should be ("r1 r2 r4")
    results2.events should be (Vector(Append("r2"), Append("r4")))
  }

  it should "return itself if None is the result of an event function" in {
    val context = createContext("r1")
    val results1 = context.resultBuilder
    results1 |> { r => None } should be theSameInstanceAs(results1)
  }

  it should "apply and append an event if Some is the result of event function" in {
    val context = createContext("r1")
    val event = Append("r2")
    val results1 = context.resultBuilder
    val results2 = results1 |> { r => Some(event) }
    results2.state should be ("r1 r2")
    results2.events should be (Vector(event))
  }

  it should "apply and append if an event object is the result of event function" in {
    val context = createContext("r1")
    val event = Append("r2")
    val results1 = context.resultBuilder
    val results2 = results1 |> { r => event }
    results2.state should be ("r1 r2")
    results2.events should be (Vector(event))
  }

  it should "allow for long pipelines of event functions" in {
    val context = createContext("r1")
    val results1 = context.resultBuilder
    val results2 = results1
      .|> { r => Append("r2") }
      .|> { r => None }
      .|> { r => Some(Append("r4")) }

    results2.state should be ("r1 r2 r4")
    results2.events should be (Vector(Append("r2"), Append("r4")))
  }

  it should "implicitly convert to a command result" in {
    val context = createContext("r1")
    val results1 = context.resultBuilder
    val commandResult: Result = results1
    commandResult should be (Root)
  }
}

