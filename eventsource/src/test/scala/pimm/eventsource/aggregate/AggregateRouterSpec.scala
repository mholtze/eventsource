package pimm.eventsource.aggregate

import akka.actor.{Terminated, Actor, ActorRef, Props}
import akka.testkit.{TestActorRef, TestProbe}
import pimm.eventsource.BaseActorTest
import scala.concurrent.duration._

class AggregateRouterSpec extends BaseActorTest {

  object CompleteShutdown

  class AggregateActor(val probe: ActorRef) extends Actor {
    override def receive: Receive = {
      case AggregateActor.Shutdown =>
        probe ! AggregateActor.Shutdown
      case CompleteShutdown =>
        probe ! CompleteShutdown
        context.stop(self)
      case x =>
        probe ! x
    }
  }

  class AggregateWapper(aggregateProps: Props) extends Actor {
    val root = context.actorOf(aggregateProps)
    context.watch(root)
    override def receive = {
        case Terminated(root) => context.stop(self)
        case x => root.forward(x)
    }
  }

  class WrappingRouter(aggregateMapFunction: ActorRef => AggregateMap) extends AggregateRouter(aggregateMapFunction) {
    override def createProps(props: Props, aggregateId: String): Props = Props(new AggregateWapper(props))
  }

  case class Msg(aggregateId: String, message: Any, props: Props) extends AggregateRouterMessage

  "An AggregateRouter" should "create a new aggregate actor when none is cached" in {
    val router = system.actorOf(Props(new AggregateRouter(ref => new GuavaAggregateMap(ref, 10L))))
    val probe = TestProbe()

    router ! Msg("agg-1", "msg1", Props(new AggregateActor(probe.ref)))
    probe.expectMsg(1 second, "msg1")
    ()
  }

  it should "reuse an existing aggregate actor if it exists" in {
    val router = system.actorOf(Props(new AggregateRouter(ref => new GuavaAggregateMap(ref, 10L))))
    val probe = TestProbe()

    router ! Msg("agg-1", "msg1", Props(new AggregateActor(probe.ref)))
    router ! Msg("agg-1", "msg2", Props(new AggregateActor(TestProbe().ref)))

    probe.expectMsg(1 second, "msg1")
    probe.expectMsg(1 second, "msg2")
    ()
  }

  it should "select an actor based on aggregate ID" in {
    val router = system.actorOf(Props(new AggregateRouter(ref => new GuavaAggregateMap(ref, 10L))))
    val probe1 = TestProbe()
    val probe2 = TestProbe()

    router ! Msg("agg-1", "msg1", Props(new AggregateActor(probe1.ref)))
    router ! Msg("agg-2", "msg2", Props(new AggregateActor(probe2.ref)))
    router ! Msg("agg-1", "msg3", Props(new AggregateActor(TestProbe().ref)))
    router ! Msg("agg-2", "msg4", Props(new AggregateActor(TestProbe().ref)))

    probe1.expectMsg(1 second, "msg1")
    probe2.expectMsg(1 second, "msg2")
    probe1.expectMsg(1 second, "msg3")
    probe2.expectMsg(1 second, "msg4")
    ()
  }

  it should "gracefully shutdown actors expelled from the cache" in {
    val router = TestActorRef(new AggregateRouter(ref => new GuavaAggregateMap(ref, 2L)))
    val probes = Vector(TestProbe(), TestProbe(), TestProbe())
    val deathWatch = TestProbe()

    router ! Msg("agg-1", "msg1", Props(new AggregateActor(probes(0).ref)))
    val agg1Ref = router.underlyingActor.aggregateMap("agg-1").get
    deathWatch watch(agg1Ref)

    router ! Msg("agg-2", "msg2", Props(new AggregateActor(probes(1).ref)))
    router ! Msg("agg-3", "msg3", Props(new AggregateActor(probes(2).ref)))

    probes(0).expectMsg(1 second, "msg1")
    probes(0).expectMsg(1 second, AggregateActor.Shutdown)

    val map = router.underlyingActor.aggregateMap
    map("agg-1") should be (None)
    map("agg-2").get.path.name should be ("agg-2")
    map("agg-3").get.path.name should be ("agg-3")

    router.underlyingActor.shuttingDown.size should be (1)
    router.underlyingActor.shuttingDown.contains("agg-1") should be (true)
    router.underlyingActor.reverseLookup(agg1Ref) should be ("agg-1")

    agg1Ref ! CompleteShutdown
    deathWatch.expectTerminated(agg1Ref, 1 second)
    router.underlyingActor.shuttingDown.size should be (0)
    router.underlyingActor.reverseLookup.get(agg1Ref) should be (None)
    router.underlyingActor.reverseLookup(map("agg-2").get) should be ("agg-2")
    router.underlyingActor.reverseLookup(map("agg-3").get) should be ("agg-3")
  }

  it should "resend pending messages to actors being shutdown" in {
    val router = TestActorRef(new AggregateRouter(ref => new GuavaAggregateMap(ref, 2L)))
    val probes = Vector(TestProbe(), TestProbe(), TestProbe())
    val props1 = Props(new AggregateActor(probes(0).ref))

    router ! Msg("agg-1", "msg1", props1)
    val agg1Ref = router.underlyingActor.aggregateMap("agg-1").get

    router ! Msg("agg-2", "msg2", Props(new AggregateActor(probes(1).ref)))
    router ! Msg("agg-3", "msg3", Props(new AggregateActor(probes(2).ref)))

    probes(0).expectMsg(1 second, "msg1")
    probes(0).expectMsg(1 second, AggregateActor.Shutdown)
    router ! Msg("agg-1", "msg4", props1)
    router ! Msg("agg-1", "msg5", props1)

    router.underlyingActor.shuttingDown.size should be (1)
    router.underlyingActor.shuttingDown("agg-1").size should be (2)

    agg1Ref ! CompleteShutdown
    probes(0).expectMsg(1 second, CompleteShutdown)
    probes(0).expectMsg(1 second, "msg4")
    probes(0).expectMsg(1 second, "msg5")

    // check that actor allocations are properly updated
    val map = router.underlyingActor.aggregateMap
    map("agg-1").get.path.name should be ("agg-1")
    map("agg-2") should be (None)
    map("agg-3").get.path.name should be ("agg-3")
    router.underlyingActor.shuttingDown.size should be (1)
    router.underlyingActor.shuttingDown.contains("agg-2") should be (true)
  }

  it should "handle unexpected actor shutdowns" in {
    val router = TestActorRef(new AggregateRouter(ref => new GuavaAggregateMap(ref, 2L)))
    val probes = Vector(TestProbe(), TestProbe(), TestProbe())
    val deathWatch = TestProbe()

    router ! Msg("agg-1", "msg1", Props(new AggregateActor(probes(0).ref)))
    router ! Msg("agg-2", "msg2", Props(new AggregateActor(probes(1).ref)))
    val agg1Ref = router.underlyingActor.aggregateMap("agg-1").get
    deathWatch watch(agg1Ref)

    agg1Ref ! CompleteShutdown

    probes(0).expectMsg(1 second, "msg1")
    probes(0).expectMsg(1 second, CompleteShutdown)
    deathWatch.expectTerminated(agg1Ref, 1 second)
    probes(0).expectNoMsg(1 milli)

    val map = router.underlyingActor.aggregateMap
    map("agg-1") should be (None)
    map("agg-2").get.path.name should be ("agg-2")
    router.underlyingActor.shuttingDown.size should be (0)
    router.underlyingActor.reverseLookup.get(agg1Ref) should be (None)
    router.underlyingActor.reverseLookup(map("agg-2").get) should be ("agg-2")
  }

  it should "seemlessly handle wrapped aggregate actors" in {
    val router = TestActorRef(new WrappingRouter(ref => new GuavaAggregateMap(ref, 2L)))
    val probes = Vector(TestProbe(), TestProbe(), TestProbe())
    val deathWatch = TestProbe()

    router ! Msg("agg-1", "msg1", Props(new AggregateActor(probes(0).ref)))
    val agg1Ref = router.underlyingActor.aggregateMap("agg-1").get
    deathWatch watch(agg1Ref)

    router ! Msg("agg-2", "msg2", Props(new AggregateActor(probes(1).ref)))
    router ! Msg("agg-3", "msg3", Props(new AggregateActor(probes(2).ref)))

    probes(0).expectMsg(1 second, "msg1")
    probes(0).expectMsg(1 second, AggregateActor.Shutdown)

    val map = router.underlyingActor.aggregateMap
    map("agg-1") should be (None)
    map("agg-2").get.path.name should be ("agg-2")
    map("agg-3").get.path.name should be ("agg-3")

    router.underlyingActor.shuttingDown.size should be (1)
    router.underlyingActor.shuttingDown.contains("agg-1") should be (true)
    router.underlyingActor.reverseLookup(agg1Ref) should be ("agg-1")

    agg1Ref ! CompleteShutdown
    deathWatch.expectTerminated(agg1Ref, 1 second)
    router.underlyingActor.shuttingDown.size should be (0)
    router.underlyingActor.reverseLookup.get(agg1Ref) should be (None)
    router.underlyingActor.reverseLookup(map("agg-2").get) should be ("agg-2")
    router.underlyingActor.reverseLookup(map("agg-3").get) should be ("agg-3")
  }
}
