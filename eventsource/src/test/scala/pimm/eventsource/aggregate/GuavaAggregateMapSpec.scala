package pimm.eventsource.aggregate

import akka.testkit.TestProbe
import pimm.eventsource.BaseActorTest
import scala.concurrent.duration._

class GuavaAggregateMapSpec extends BaseActorTest {
  import AggregateRouter._

  "An AggregateMap" should "track actors by aggregate ID" in {
    val router = TestProbe()
    val map = new GuavaAggregateMap(router.ref, 10L)
    val actor = TestProbe()
    map += "agg-1" -> actor.ref

    map("agg-1").get should be theSameInstanceAs actor.ref
    map("agg-2") should be (None)
  }

  it should "invalidate removed entries" in {
    val router = TestProbe()
    val map = new GuavaAggregateMap(router.ref, 2L)
    val actors = Vector(TestProbe(), TestProbe(), TestProbe())

    map += "agg-1" -> actors(0).ref
    map += "agg-2" -> actors(1).ref
    map -= "agg-1"

    map("agg-1") should be (None)
    map("agg-2").get should be theSameInstanceAs actors(1).ref

    router.expectNoMsg(100 milli)
    //router.expectMsg(1 second, ShutdownAggregate("agg-1", actors(0).ref))
    ()
  }

  it should "maintain finite number of actors" in {
    val router = TestProbe()
    val map = new GuavaAggregateMap(router.ref, 2L)
    val actors = Vector(TestProbe(), TestProbe(), TestProbe())

    map += "agg-1" -> actors(0).ref
    map += "agg-2" -> actors(1).ref
    map += "agg-3" -> actors(2).ref

    map("agg-1") should be (None)
    map("agg-2").get should be theSameInstanceAs actors(1).ref
    map("agg-3").get should be theSameInstanceAs actors(2).ref

    router.expectMsg(1 second, ShutdownAggregate("agg-1", Some(actors(0).ref)))
    ()
  }

  it should "expel actors after a timeout duration since last access" in {
    val router = TestProbe()
    val map = new GuavaAggregateMap(router.ref, 2L, Some(100 milli))
    val actors = Vector(TestProbe(), TestProbe())

    map += "agg-1" -> actors(0).ref
    Thread.sleep(200)
    map += "agg-2" -> actors(1).ref

    map("agg-1") should be (None)
    map("agg-2").get should be theSameInstanceAs actors(1).ref

    router.expectMsg(1 second, ShutdownAggregate("agg-1", Some(actors(0).ref)))
    ()
  }
}
