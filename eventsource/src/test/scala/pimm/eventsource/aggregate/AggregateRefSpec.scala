package pimm.eventsource.aggregate

import akka.actor.{Actor, ActorRef}
import pimm.eventsource.EventSourceSystem

import scala.concurrent.Await
import scala.concurrent.duration._
import scala.reflect.ClassTag

class AggregateRefSpec extends BaseAggregateActorTest {
  import BaseAggregateActorTest._
  import CommandRequest._
  import CommandReply._

  def timeout: FiniteDuration = 1 second

  def aggregateRef(eventSystem: EventSourceSystem, aggregateId: String): AggregateRef[String] = new AggregateRef[String] {
    override def createRouterMessage(message: Any) = newMessage(aggregateId, message)
    override def router: ActorRef = eventSystem.aggregateRouter
    override def provider = ???
  }

  "An AggregateRef" should "send message using '!'" in withSystem(configWithEventProbe) { (system, eventSystem) =>
    val probe = ProbingConsistencyBuilder(system).probe
    val aggRef = aggregateRef(eventSystem, "agg-1")
    aggRef ! AppendCommand("r2")
    probe.expectMsg(timeout, Append("r2"))
    ()
  }

  it should "send message using 'tell'" in withSystem(configWithEventProbe) { (system, eventSystem) =>
    val probe = ProbingConsistencyBuilder(system).probe
    val aggRef = aggregateRef(eventSystem, "agg-1")
    aggRef.tell(AppendCommand("r2"), Actor.noSender)
    probe.expectMsg(timeout, Append("r2"))
    ()
  }

  it should "ask using '?'" in withSystem { (system, eventSystem) =>
    val aggRef = aggregateRef(eventSystem, "agg-1")
    val future = aggRef ? Reply(StandardCommands.Root)
    val result = Await.result(future, timeout)
    result should be (RootRef("r1"))
    ()
  }

  it should "ask using 'ask'" in withSystem { (system, eventSystem) =>
    val aggRef = aggregateRef(eventSystem, "agg-1")
    val future = aggRef ask Reply(StandardCommands.Root)
    val result = Await.result(future, timeout)
    result should be (RootRef("r1"))
    ()
  }

  it should "auto-wrap with reply using '?'" in withSystem { (system, eventSystem) =>
    val aggRef = aggregateRef(eventSystem, "agg-1")
    val future = aggRef ? StandardCommands.Root
    val result = Await.result(future, timeout)
    result should be (RootRef("r1"))
    ()
  }

  it should "auto-wrap with reply using 'ask'" in withSystem { (system, eventSystem) =>
    val aggRef = aggregateRef(eventSystem, "agg-1")
    val future = aggRef ask StandardCommands.Root
    val result = Await.result(future, timeout)
    result should be (RootRef("r1"))
    ()
  }

  it should "retrieve the root as a value" in withSystem { (system, eventSystem) =>
    val aggRef = aggregateRef(eventSystem, "agg-1")
    val future = aggRef.root
    val result = Await.result(future, timeout)
    result should be ("r1")
    ()
  }

  it should "retrieve the root as an Option" in withSystem { (system, eventSystem) =>
    val aggRef = aggregateRef(eventSystem, "agg-1")
    val future = aggRef.rootOption
    val result = Await.result(future, timeout)
    result should be (Some("r1"))
    ()
  }

  it should "support ask with an unboxed result" in withSystem { (system, eventSystem) =>
    val aggRef = aggregateRef(eventSystem, "agg-1")
    val future = aggRef.askUnboxed(StandardCommands.Root)
    val result = Await.result(future, timeout)
    result should be ("r1")
    ()
  }

  it should "support ask with an Option result" in withSystem { (system, eventSystem) =>
    val aggRef = aggregateRef(eventSystem, "agg-1")
    val future = aggRef.askOption(StandardCommands.Root)
    val result = Await.result(future, timeout)
    result should be (Some("r1"))
    ()
  }

  it should "support ask with a consistency acknowledgment" in withSystem(configWithEventProbe) { (system, eventSystem) =>
    val probe = ProbingConsistencyBuilder(system).probe
    val aggRef = aggregateRef(eventSystem, "agg-1")
    val future = aggRef.askConsistent(AppendCommand("r2"))
    probe.expectMsg(timeout, Append("r2"))

    val result = Await.result(future, timeout)
    result should be (Success("r1 r2", Vector(Append("r2"))))
    ()
  }
}
