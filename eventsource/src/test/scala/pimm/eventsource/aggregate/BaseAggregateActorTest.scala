package pimm.eventsource.aggregate

import akka.actor.{Props, ActorSystem}
import akka.util.Timeout
import com.typesafe.config.{Config, ConfigFactory}
import org.scalatest.{Matchers, FlatSpecLike}
import pimm.eventsource.EventSourceSystem

import scala.concurrent.duration.FiniteDuration

object BaseAggregateActorTest {

  class StringAggregate(val aggregateId: String) extends DirectStateAggregate[String] {
    var state: String = "r1"

    def applyEvent(state: String, seqNr: Long) = {
      case Append(value) => state match {
        case null => value
        case _ => state + " " + value
      }
    }

    def executeCommand = {
      case AppendCommand(values) => commandResult ++ values.map(Append(_))
      case ErrorCommand => throw new NotImplementedError("ErrorCommand")
      case RejectCommand => commandResult.reject("RejectCommand")
    }
  }

  object StringAggregate extends AggregateProps[String] {
    override def props(aggregateId: String): Props = Props(new StringAggregate(aggregateId))
  }

  case class Append(value: String)
  case class AppendCommand(values: Seq[String])

  object AppendCommand {
    def apply(value: String): AppendCommand = AppendCommand(Seq(value))
  }

  case object ErrorCommand
  case object RejectCommand
}

abstract class BaseAggregateActorTest extends FlatSpecLike with Matchers {
  import BaseAggregateActorTest._

  def config = ConfigFactory.parseString("""
    |akka.loggers = ["akka.testkit.TestEventListener"]
    |akka.persistence.journal.plugin = "pimm.eventsource.inmem.journal"
    |akka.persistence.snapshot-store.plugin = "pimm.eventsource.inmem.snapshot"
    |akka.persistence.journal.leveldb.native = off
    |""".stripMargin).withFallback(ConfigFactory.defaultReference())

  def configWithEventProbe = ConfigFactory.parseString("""
    |pimm.eventsource.events.eventuals.consistency-builder-class = "pimm.eventsource.aggregate.EventProbeConsistencyBuilder"
    |""".stripMargin).withFallback(config)

  def timeout: FiniteDuration
  implicit val futureTimeout = Timeout(timeout)


  def withSystem[T](config: Config)(testCode: (ActorSystem, EventSourceSystem) => Any): Unit = {
    val system: ActorSystem = ActorSystem("unit-test", config)
    val eventSystem = EventSourceSystem(system)
    try {
      testCode(system, eventSystem)
      ()
    } finally {
      system.terminate()
      ()
    }
  }

  def withSystem[T](testCode: (ActorSystem, EventSourceSystem) => Any): Unit = withSystem(config)(testCode)

  def newMessage(aggregateId: String, message: Any) =
    AggregatePropsMessage(Props(new StringAggregate(aggregateId)), aggregateId, message)

}
