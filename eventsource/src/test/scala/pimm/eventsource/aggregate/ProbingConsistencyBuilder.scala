package pimm.eventsource.aggregate

import akka.actor._
import akka.testkit.TestProbe
import pimm.eventsource.events.{Eventuals, PersistentEventEnvelope}

object ProbingConsistencyBuilder extends ExtensionId[ProbingConsistencyBuilder] with ExtensionIdProvider {
  override def lookup = ProbingConsistencyBuilder
  override def createExtension(system: ExtendedActorSystem) = new ProbingConsistencyBuilder(system)
  override def get(system: ActorSystem): ProbingConsistencyBuilder = super.get(system)
}

final class ProbingConsistencyBuilder(system: ActorSystem) extends Extension {
  val probe = TestProbe()(system)
}

final class EnvelopeProbeConsistencyBuilder(val eventuals: ActorRef) extends Actor {
  import Eventuals._

  val ext = ProbingConsistencyBuilder(context.system)

  override def receive = {
    case env: PersistentEventEnvelope =>
      ext.probe.ref ! env
      eventuals ! EventAck(env)
  }
}

final class EventProbeConsistencyBuilder(val eventuals: ActorRef) extends Actor {
  import Eventuals._

  val ext = ProbingConsistencyBuilder(context.system)

  override def receive = {
    case env: PersistentEventEnvelope =>
      ext.probe.ref ! env.event
      eventuals ! EventAck(env)
  }
}