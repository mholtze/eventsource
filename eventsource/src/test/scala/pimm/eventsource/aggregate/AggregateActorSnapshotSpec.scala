package pimm.eventsource.aggregate

import akka.persistence.SelectedSnapshot
import akka.testkit.{EventFilter, TestProbe}
import com.typesafe.config.{ConfigFactory, Config}
import pimm.eventsource.inmem.InmemStoreExtension

import scala.collection.IndexedSeq
import scala.concurrent.Await
import scala.concurrent.duration._
import akka.pattern.ask


class AggregateActorSnapshotSpec extends BaseAggregateActorTest {
  import BaseAggregateActorTest._
  import CommandRequest._
  import AggregateRouter._

  def timeout: FiniteDuration = 1 second

  override def config: Config = ConfigFactory.parseString("""
    |pimm.eventsource.aggregate.max-events-snapshot-strategy.max-events = 2
    |pimm.eventsource.aggregate.max-events-snapshot-strategy.retry-attempts = 2
    |pimm.eventsource.aggregate.max-events-snapshot-strategy.retry-delay = 10ms
    |""".stripMargin).withFallback(super.config)

  "An AggregateActor" should "take period snapshots" in withSystem { (system, eventSystem) =>
    val probe = TestProbe()(system)
    val inmemStore = InmemStoreExtension(system).inmemStore
    system.eventStream.subscribe(probe.ref, classOf[SelectedSnapshot])

    val msg = newMessage("agg-1", ReplyConsistent(AppendCommand(Seq("r2", "r3", "r4"))))
    Await.result(eventSystem.aggregateRouter ? msg, timeout)
    probe.expectMsgClass[SelectedSnapshot](timeout, classOf[SelectedSnapshot])
    inmemStore.readSnapshots("agg-1").head.snapshot should be ("r1 r2 r3 r4")
  }

  it should "take snapshot on recovery if needed" in withSystem { (system, eventSystem) =>
    val probe = TestProbe()(system)
    val inmemStore = InmemStoreExtension(system).inmemStore
    system.eventStream.subscribe(probe.ref, classOf[SelectedSnapshot])

    val msg = newMessage("agg-1", ReplyConsistent(AppendCommand(Seq("r2", "r3", "r4"))))
    Await.result(eventSystem.aggregateRouter ? msg, timeout)
    probe.expectMsgClass[SelectedSnapshot](timeout, classOf[SelectedSnapshot])

    // delete snapshot from inmemStore
    inmemStore.deleteSnapshot(inmemStore.readSnapshots("agg-1").head.metadata)
    inmemStore.readSnapshots("agg-1") should be (IndexedSeq())
    // shutdown aggregate actor
    eventSystem.aggregateRouter ! ShutdownAggregate("agg-1")
    //send root read message to restart actor
    Await.result(eventSystem.aggregateRouter ? newMessage("agg-1", Reply(StandardCommands.Root)), timeout)

    probe.expectMsgClass[SelectedSnapshot](timeout, classOf[SelectedSnapshot])
    inmemStore.readSnapshots("agg-1").head.snapshot should be ("r1 r2 r3 r4")
  }

  it should "retry snapshots on failure" in withSystem { (system, eventSystem) =>
    implicit val actorSystem = system
    val inmemStore = InmemStoreExtension(system).inmemStore
    inmemStore.snapshotsEnabled = false

    EventFilter.warning(pattern="Failed to saveSnapshot given metadata .*", occurrences = 2) intercept {
      val msg = newMessage("agg-1", AppendCommand(Seq("r2", "r3", "r4")))
      eventSystem.aggregateRouter ! msg
    }
  }

  it should "not exceed max snapshot retries" in withSystem { (system, eventSystem) =>
    implicit val actorSystem = system
    val inmemStore = InmemStoreExtension(system).inmemStore
    inmemStore.snapshotsEnabled = false

    EventFilter.warning(pattern="Save snapshot retries exhausted for metadata .*", occurrences = 1) intercept {
      EventFilter.warning(pattern="Failed to saveSnapshot given metadata .*", occurrences = 2) intercept {
        val msg = newMessage("agg-1", AppendCommand(Seq("r2", "r3", "r4")))
        eventSystem.aggregateRouter ! msg
      }
    }
  }
}
