package pimm.eventsource.aggregate

import scala.concurrent.Await
import scala.concurrent.duration.FiniteDuration
import scala.concurrent.duration._

class AggregateRefProviderSpec extends BaseAggregateActorTest {
  import BaseAggregateActorTest._

  def timeout: FiniteDuration = 1 second

  "An AggregateRefProvider" should "route to an aggregate using an AggregateProps provider" in withSystem { (system, eventSystem) =>
    val aggRef = eventSystem.to(StringAggregate, "agg-1")
    val future = aggRef.root
    val result = Await.result(future, timeout)
    result should be ("r1")
    ()
  }

 it should "route to an aggregate using an Actor's type" in withSystem { (system, eventSystem) =>
    val aggRef = eventSystem.to[StringAggregate, String]("agg-1")
    val future = aggRef.root
    val result = Await.result(future, timeout)
    result should be ("r1")
    ()
  }

  it should "route to an aggregate using an Actor's type from classOf" in withSystem { (system, eventSystem) =>
    val aggRef = eventSystem.to[String](classOf[StringAggregate], "agg-1")
    val future = aggRef.root
    val result = Await.result(future, timeout)
    result should be ("r1")
    ()
  }

  it should "pipe '|>' to an aggregate using an AggregateProps provider" in withSystem { (system, eventSystem) =>
    val future = eventSystem |> (StringAggregate, "agg-1") root
    val result = Await.result(future, timeout)
    result should be ("r1")
    ()
  }

  it should "pipe '|>' to an aggregate using an Actor's type" in withSystem { (system, eventSystem) =>
    val future = eventSystem |> [StringAggregate, String] "agg-1" root
    val result = Await.result(future, timeout)
    result should be ("r1")
    ()
  }
}