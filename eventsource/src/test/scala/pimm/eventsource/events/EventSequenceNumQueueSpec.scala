package pimm.eventsource.events

import pimm.eventsource.BaseActorTest
import scala.concurrent.duration._

class EventSequenceNumQueueSpec extends BaseActorTest {
  case class BasicEvent(value: String)

  case class EnvelopeImpl(
    event: Any,
    sequenceNr: Long,
    aggregateId: String = "agg-1"
  ) extends PersistentEventEnvelope {
    def globalSequenceNr: Long = sequenceNr
  }

  "An EventSequenceNumQueue" should "add a pending entry and sequence for pending events" in {
    val queue = new EventSequenceNumQueue((60 minutes).toMillis)
    val env = EnvelopeImpl(BasicEvent("v1"), 1)
    queue.enqueuePending(env)

    val pending = queue.pendingSeqNumbers().head
    pending.seqNum should be (1)
    pending.envelope should be theSameInstanceAs env
    pending.acknowledged should be (false)

    queue.pendingEvents(env.globalSequenceNr) should be theSameInstanceAs pending
  }

  it should "handle events of any type" in {
    val queue = new EventSequenceNumQueue((60 minutes).toMillis)
    val env = EnvelopeImpl("v1", 1) // just use a string message instead of one that extends Event
    queue.enqueuePending(env)

    val pending = queue.pendingSeqNumbers().head
    pending.seqNum should be (1)
    pending.envelope should be theSameInstanceAs env
    pending.acknowledged should be (false)

    queue.pendingEvents(env.globalSequenceNr) should be theSameInstanceAs pending
  }

  it should "remove the pending entry and set acknowledge to true upon acknowledgement" in {
    val queue = new EventSequenceNumQueue((60 minutes).toMillis)
    val envs = Vector(BasicEvent("v1"), BasicEvent("v2"), BasicEvent("v3"))
      .zipWithIndex.map(x => EnvelopeImpl(x._1, x._2 + 1L))

    envs.foreach(queue.enqueuePending)
    queue.acknowledge(envs(1).globalSequenceNr)
    val pending = queue.pendingSeqNumbers()
    val pendingEvents = queue.pendingEvents

    pending.map(x => x.acknowledged) should be (Vector(false, true, false))
    pendingEvents(envs(0).globalSequenceNr).envelope should be theSameInstanceAs envs(0)
    pendingEvents.get(envs(1).globalSequenceNr) should be (None)
    pendingEvents(envs(2).globalSequenceNr).envelope should be theSameInstanceAs envs(2)
  }

  it should "dequeue all if all are acknowledged" in {
    val queue = new EventSequenceNumQueue((60 minutes).toMillis)
    val envs = Vector(BasicEvent("v1"), BasicEvent("v2"))
      .zipWithIndex.map(x => EnvelopeImpl(x._1, x._2 + 1L))

    envs.foreach(queue.enqueuePending)
    envs.foreach(e => queue.acknowledge(e.globalSequenceNr))

    val pending = queue.pendingSeqNumbers()
    val pendingEvents = queue.pendingEvents
    pending.map(x => x.acknowledged) should be (Vector(true, true))
    pendingEvents.size should be (0)

    queue.dequeueMax().get should be (2)
    queue.pendingSeqNumbers().size should be (0)
  }

  it should "dequeue up to the maximum acknowledged event if there are unacknowledged" in {
    val queue = new EventSequenceNumQueue((60 minutes).toMillis)
    val envs = Vector(BasicEvent("v1"), BasicEvent("v2"), BasicEvent("v3"), BasicEvent("v4"))
      .zipWithIndex.map(x => EnvelopeImpl(x._1, x._2 + 1L))

    envs.foreach(queue.enqueuePending)
    envs.filter(_.event.asInstanceOf[BasicEvent].value != "v3").foreach(e => queue.acknowledge(e.globalSequenceNr))

    queue.pendingSeqNumbers().map(x => x.acknowledged) should be (Vector(true, true, false, true))
    queue.pendingEvents.size should be (1)
    queue.pendingEvents(envs(2).globalSequenceNr).envelope should be theSameInstanceAs envs(2)

    queue.dequeueMax().get should be (2)
    queue.pendingSeqNumbers().map(x => x.acknowledged) should be (Vector(false, true))
  }

  it should "timeout after a specified timeout period" in {
    val queue = new EventSequenceNumQueue((100 milliseconds).toMillis)
    val envs = Vector(BasicEvent("v1"), BasicEvent("v2"))
      .zipWithIndex.map(x => EnvelopeImpl(x._1, x._2 + 1L))

    queue.enqueuePending(envs(0))
    Thread.sleep(150)
    queue.enqueuePending(envs(1))

    val timeoutes = queue.checkTimeouts()
    val pending = queue.pendingSeqNumbers()
    val pendingEvents = queue.pendingEvents
    pending.map(x => x.acknowledged) should be (Vector(false, false))
    pending.map(x => x.timedOut) should be (Vector(true, false))
    pendingEvents.size should be (2)
    pendingEvents(envs(0).globalSequenceNr).envelope should be theSameInstanceAs envs(0)
    pendingEvents(envs(1).globalSequenceNr).envelope should be theSameInstanceAs envs(1)
    timeoutes should be (List(envs(0)))
  }

  it should "not timeout twice" in {
    val queue = new EventSequenceNumQueue((100 milliseconds).toMillis)
    val envs = Vector(BasicEvent("v1"), BasicEvent("v2"))
      .zipWithIndex.map(x => EnvelopeImpl(x._1, x._2 + 1L))

    queue.enqueuePending(envs(0))
    Thread.sleep(150)
    queue.enqueuePending(envs(1))

    val timeoutes1 = queue.checkTimeouts()
    timeoutes1 should be (List(envs(0)))

    Thread.sleep(150)
    val timeoutes2 = queue.checkTimeouts()
    timeoutes2 should be (List(envs(1)))

    val pending = queue.pendingSeqNumbers()
    pending.map(x => x.acknowledged) should be (Vector(false, false))
    pending.map(x => x.timedOut) should be (Vector(true, true))
  }

  it should "allow acknowledgement after timeout" in {
    val queue = new EventSequenceNumQueue((50 milliseconds).toMillis)
    val envs = Vector(BasicEvent("v1"))
      .zipWithIndex.map(x => EnvelopeImpl(x._1, x._2 + 1L))

    queue.enqueuePending(envs(0))
    Thread.sleep(100)
    queue.checkTimeouts()
    queue.acknowledge(envs(0).globalSequenceNr)

    val pending = queue.pendingSeqNumbers()
    val pendingEvents = queue.pendingEvents
    pending.map(x => x.acknowledged) should be (Vector(true))
    pending.map(x => x.timedOut) should be (Vector(true))
    pendingEvents.size should be (0)

    queue.dequeueMax().get should be (1)
    queue.pendingSeqNumbers().size should be (0)
  }
}
