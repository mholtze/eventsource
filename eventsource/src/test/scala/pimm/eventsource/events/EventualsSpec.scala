package pimm.eventsource.events

import akka.actor.{Status, Props, Actor, ActorRef}
import akka.stream.actor.ActorPublisher
import akka.stream.scaladsl.Source
import akka.testkit.TestProbe
import pimm.eventsource.BaseActorTest
import scala.collection.immutable
import scala.concurrent.duration._

import scala.concurrent.Future

class EventualsSpec extends BaseActorTest {
  import Eventuals._

  case class BasicEvent(value: String)

  case class EnvelopeImpl(
    offset: Long,
    event: Any,
    aggregateId: String = "agg-1"
  ) extends PersistentEventEnvelope {
    override def sequenceNr: Long = offset
    override def globalSequenceNr: Long = offset
  }

  object AllEventsPublisher {
    final case class NextEvent(envelope: EnvelopeImpl)
    object Complete
    object Error
  }

  class AllEventsPublisher(initialSeqNum: Long = 0L) extends ActorPublisher[PersistentEventEnvelope] {
    import akka.stream.actor.ActorPublisherMessage._

    var buf = Vector.empty[PersistentEventEnvelope]
    var seqNum: Long = initialSeqNum

    def receive = {
      case next: AllEventsPublisher.NextEvent =>
        seqNum = next.envelope.globalSequenceNr
        if (buf.isEmpty && totalDemand > 0)
          onNext(next.envelope)
        else {
          buf :+= next.envelope
          deliverBuf()
        }
      case AllEventsPublisher.Complete => onComplete()
      case AllEventsPublisher.Error => onError(new RuntimeException("Error Test"))

      case Request(_) => deliverBuf()
      case Cancel => context.stop(self)
    }

    def deliverBuf(): Unit =
      if (totalDemand > 0) {
        val (use, keep) = buf.splitAt(totalDemand.toInt)
        buf = keep
        use foreach onNext
      }
  }

  class EnvelopeSeq(initialSeqNum: Long = 0L) {
    private var curSeqNum = initialSeqNum

    def seqNum = curSeqNum
    def next(event: Any) = {
      curSeqNum += 1
      EnvelopeImpl(curSeqNum, event)
    }
  }

  class EventualsApiImpl(val notifier: ActorRef, initialLastSeqNr: Long = 0L) extends EventualsApi {
    private var lastSeqNr: Long = initialLastSeqNr
    var publisher: ActorRef = _

    override def readLastGlobalSeqNr(): Future[Long] = {
      notifier ! (("readLastGlobalSeqNr", lastSeqNr))
      Future.successful(lastSeqNr)
    }

    override def saveLastGlobalSeqNr(lastSeqNr: Long): Unit = {
      this.lastSeqNr = lastSeqNr
      notifier ! (("saveLastGlobalSeqNr", lastSeqNr))
    }

    override def allEventsLive(globalFrom: Long): Source[PersistentEventEnvelope, Unit] = {
      Source
        .actorPublisher[PersistentEventEnvelope](Props(new AllEventsPublisher(globalFrom-1)))
        .mapMaterializedValue(a => {
          publisher = a
          notifier ! (("streaming", globalFrom))
        })
    }
  }

  class ConsistencyBuilderProbe(val probe: ActorRef, val eventuals: ActorRef) extends Actor {
    override def receive = {
      case x => probe forward x
    }
  }

  case class EventsResults(events: immutable.Seq[Any])

  def implFunc(impl: EventualsApiImpl): ImplFunction = (_, _) => impl

  "Eventuals" should "read the lastGlobalSeqNr at startup" in {
    val notify = TestProbe()
    val impl = new EventualsApiImpl(notify.ref, 2L)
    val eventuals = system.actorOf(Props(new Eventuals(implFunc(impl))))
    notify.expectMsg(1 second, ("readLastGlobalSeqNr", 2L))
    ()
  }

  it should "make a connection to allEvents at startup" in {
    val notify = TestProbe()
    val impl = new EventualsApiImpl(notify.ref, 2L)
    val eventuals = system.actorOf(Props(new Eventuals(implFunc(impl))))
    notify.expectMsg(1 second, ("readLastGlobalSeqNr", 2L))
    notify.expectMsg(1 second, ("streaming", 3L))
    ()
  }

  it should "restart allEvents from the lastGlobalSeqNr if it fails" in {
    val notify = TestProbe()
    val impl = new EventualsApiImpl(notify.ref, 2L)
    val eventuals = system.actorOf(Props(new Eventuals(implFunc(impl), allEventsRetryDelay=10)))
    notify.expectMsg(1 second, ("readLastGlobalSeqNr", 2L))
    notify.expectMsg(1 second, ("streaming", 3L))
    impl.publisher ! AllEventsPublisher.Complete
    notify.expectMsg(1 second, ("streaming", 3L))
    ()
  }

  it should "automatically acknowledge events when using the EventualConsistencyBuilder" in {
    val notify = TestProbe()
    val envSeq = new EnvelopeSeq(2L)
    val impl = new EventualsApiImpl(notify.ref, 2L)
    val eventuals = system.actorOf(Props(new Eventuals(implFunc(impl))))
    val events = Vector(BasicEvent("v1")).map(envSeq.next)
    notify.fishForMessage(1 second) {
      case ("streaming", _) => true
      case _ => false
    }
    impl.publisher ! AllEventsPublisher.NextEvent(events(0))
    notify.expectMsg(1 second, ("saveLastGlobalSeqNr", 3L))
    ()
  }

  it should "publish events of all types to the consistency builder" in {
    val notify = TestProbe()
    val probe = TestProbe()
    val envSeq = new EnvelopeSeq(2L)
    val impl = new EventualsApiImpl(notify.ref, 2L)
    val eventuals = system.actorOf(Props(new Eventuals(implFunc(impl), cl => Props(new ConsistencyBuilderProbe(probe.ref, cl)))))
    val events = Vector(BasicEvent("v1"), "v2", BasicEvent("v3")).map(envSeq.next)
    notify.fishForMessage(1 second) {
      case ("streaming", _) => true
      case _ => false
    }
    events.foreach(impl.publisher ! AllEventsPublisher.NextEvent(_))
    probe.expectMsg(1 second, events(0))
    probe.expectMsg(1 second, events(1))
    probe.expectMsg(1 second, events(2))
    ()
  }

  it should "wait for acknowledge of events" in {
    val notify = TestProbe()
    val probe = TestProbe()
    val envSeq = new EnvelopeSeq(2L)
    val impl = new EventualsApiImpl(notify.ref, 2L)
    val eventuals = system.actorOf(Props(new Eventuals(implFunc(impl), cl => Props(new ConsistencyBuilderProbe(probe.ref, cl)))))
    val events = Vector(BasicEvent("v1"), BasicEvent("v2"), BasicEvent("v3"), BasicEvent("v4")).map(envSeq.next)
    notify.fishForMessage(1 second) {
      case ("streaming", _) => true
      case _ => false
    }
    events.foreach(impl.publisher ! AllEventsPublisher.NextEvent(_))
    probe.expectMsg(1 second, events(0))
    probe.expectMsg(1 second, events(1))
    probe.expectMsg(1 second, events(2))
    probe.expectMsg(1 second, events(3))

    eventuals ! new EventAck(events(0))
    eventuals ! new EventAck(events(1))
    eventuals ! new EventAck(events(3))

    notify.expectMsg(1 second, ("saveLastGlobalSeqNr", 3L))
    notify.expectMsg(1 second, ("saveLastGlobalSeqNr", 4L))
    notify.expectNoMsg(50 milli)
    ()
  }

  it should "support acknowledging a sequence of events" in {
    val notify = TestProbe()
    val probe = TestProbe()
    val envSeq = new EnvelopeSeq(2L)
    val impl = new EventualsApiImpl(notify.ref, 2L)
    val eventuals = system.actorOf(Props(new Eventuals(implFunc(impl), cl => Props(new ConsistencyBuilderProbe(probe.ref, cl)))))
    val events = Vector(BasicEvent("v1"), BasicEvent("v2"), BasicEvent("v3"), BasicEvent("v4")).map(envSeq.next)
    notify.fishForMessage(1 second) {
      case ("streaming", _) => true
      case _ => false
    }
    events.foreach(impl.publisher ! AllEventsPublisher.NextEvent(_))
    probe.expectMsg(1 second, events(0))
    probe.expectMsg(1 second, events(1))
    probe.expectMsg(1 second, events(2))
    probe.expectMsg(1 second, events(3))

    eventuals ! new EventAckSeq(Seq(events(0), events(1), events(3)))

    notify.expectMsg(1 second, ("saveLastGlobalSeqNr", 4L))
    notify.expectNoMsg(50 milli)
    ()
  }

  it should "periodically timeout events" in {
    val notify = TestProbe()
    val probe = TestProbe()
    val envSeq = new EnvelopeSeq(2L)
    val impl = new EventualsApiImpl(notify.ref, 2L)
    val eventuals = system.actorOf(Props(new Eventuals(implFunc(impl), cl => Props(new ConsistencyBuilderProbe(probe.ref, cl)), ackTimeout = 50)))
    val events = Vector(BasicEvent("v1"), BasicEvent("v2")).map(envSeq.next)
    val events2 = Vector(BasicEvent("v3"), BasicEvent("v4")).map(envSeq.next)
    notify.fishForMessage(1 second) {
      case ("streaming", _) => true
      case _ => false
    }
    events.foreach(impl.publisher ! AllEventsPublisher.NextEvent(_))
    probe.expectMsg(1 second, events(0))
    probe.expectMsg(1 second, events(1))
    probe.expectMsg(1 second, AckTimeout(events(0)))
    probe.expectMsg(1 second, AckTimeout(events(1)))

    // do a second round to verify rescheduling of timeout
    events2.foreach(impl.publisher ! AllEventsPublisher.NextEvent(_))
    probe.expectMsg(1 second, events2(0))
    probe.expectMsg(1 second, events2(1))
    probe.expectMsg(1 second, AckTimeout(events2(0)))
    probe.expectMsg(1 second, AckTimeout(events2(1)))

    // ensure no acknowledges
    notify.expectNoMsg(50 milli)
    ()
  }

  it should "support resending all pending events" in {
    val notify = TestProbe()
    val probe = TestProbe()
    val envSeq = new EnvelopeSeq(2L)
    val impl = new EventualsApiImpl(notify.ref, 2L)
    val eventuals = system.actorOf(Props(new Eventuals(implFunc(impl), cl => Props(new ConsistencyBuilderProbe(probe.ref, cl)), ackTimeout = 100)))
    val events = Vector(BasicEvent("v1"), BasicEvent("v2")).map(envSeq.next)
    val events2 = Vector(BasicEvent("v3"), BasicEvent("v4")).map(envSeq.next)
    notify.fishForMessage(1 second) {
      case ("streaming", _) => true
      case _ => false
    }
    events.foreach(impl.publisher ! AllEventsPublisher.NextEvent(_))
    probe.expectMsg(1 second, events(0))
    probe.expectMsg(1 second, events(1))
    eventuals ! EventAck(events(1))
    probe.expectMsg(1 second, AckTimeout(events(0)))

    // do a second round to verify rescheduling of timeout
    events2.foreach(impl.publisher ! AllEventsPublisher.NextEvent(_))
    probe.expectMsg(1 second, events2(0))
    probe.expectMsg(1 second, events2(1))

    eventuals ! ResendPending
    probe.expectMsg(1 second, ResendEvent(AckTimeout(events(0))))
    probe.expectMsg(1 second, ResendEvent(events2(0)))
    probe.expectMsg(1 second, ResendEvent(events2(1)))
    probe.expectMsg(1 second, ResendComplete)
    ()
  }

  it should "acknowledge pending after processing" in {
    val notify = TestProbe()
    val probe = TestProbe()
    val envSeq = new EnvelopeSeq(2L)
    val impl = new EventualsApiImpl(notify.ref, 2L)
    val eventuals = system.actorOf(Props(new Eventuals(implFunc(impl))))
    val events = Vector(BasicEvent("v1")).map(envSeq.next)
    val result = EventsResults(events)
    notify.fishForMessage(1 second) {
      case ("streaming", _) => true
      case _ => false
    }

    eventuals ! AcknowledgeConsistent("agg-1", events(0).sequenceNr, events.size, result, probe.ref)
    impl.publisher ! AllEventsPublisher.NextEvent(events(0))
    probe.expectMsg(1 second, Status.Success(result))
    ()
  }

  it should "acknowledge pending after processing out of order" in {
    val notify = TestProbe()
    val probe = TestProbe()
    val envSeq = new EnvelopeSeq(2L)
    val impl = new EventualsApiImpl(notify.ref, 2L)
    val eventuals = system.actorOf(Props(new Eventuals(implFunc(impl), cl => Props(new ConsistencyBuilderProbe(probe.ref, cl)))))
    val events = Vector(BasicEvent("v1"), BasicEvent("v2")).map(envSeq.next)
    val result = EventsResults(events)
    notify.fishForMessage(1 second) {
      case ("streaming", _) => true
      case _ => false
    }

    eventuals ! AcknowledgeConsistent("agg-1", events(0).sequenceNr, events.size, result, probe.ref)
    events.foreach(impl.publisher ! AllEventsPublisher.NextEvent(_))

    probe.expectMsg(1 second, events(0))
    probe.expectMsg(1 second, events(1))
    eventuals ! new EventAck(events(1))
    eventuals ! new EventAck(events(0))
    probe.expectMsg(1 second, Status.Success(result))
    ()
  }

  it should "timeout unacknowledged pending" in {
    val notify = TestProbe()
    val probe = TestProbe()
    val envSeq = new EnvelopeSeq(2L)
    val impl = new EventualsApiImpl(notify.ref, 2L)
    val eventuals = system.actorOf(Props(new Eventuals(implFunc(impl), cl => Props(new ConsistencyBuilderProbe(probe.ref, cl)), ackTimeout = 100)))
    val events = Vector(BasicEvent("v1"), BasicEvent("v2")).map(envSeq.next)
    val result = EventsResults(events)
    notify.fishForMessage(1 second) {
      case ("streaming", _) => true
      case _ => false
    }

    eventuals ! AcknowledgeConsistent("agg-1", events(0).sequenceNr, events.size, result, probe.ref)
    events.foreach(impl.publisher ! AllEventsPublisher.NextEvent(_))

    probe.expectMsg(1 second, events(0))
    probe.expectMsg(1 second, events(1))
    eventuals ! new EventAck(events(1))
    probe.fishForMessage(1 seconds) {
      case Status.Failure(cause: AcknowledgedTimeoutException) =>
        cause.result should be (result)
        cause.timeout should be (Some(AckTimeout(events(0))))
        true
      case timeout: AckTimeout => false
    }
    ()
  }

  it should "handle backpressure" in {
    val notify = TestProbe()
    val probe = TestProbe()
    val envSeq = new EnvelopeSeq(2L)
    val impl = new EventualsApiImpl(notify.ref, 2L)
    val eventuals = system.actorOf(Props(new Eventuals(implFunc(impl), cl => Props(new ConsistencyBuilderProbe(probe.ref, cl)), maxPendingEvents = 1)))
    val events = Vector(BasicEvent("v1"), BasicEvent("v2")).map(envSeq.next)
    notify.fishForMessage(1 second) {
      case ("streaming", _) => true
      case _ => false
    }

    events.foreach(impl.publisher ! AllEventsPublisher.NextEvent(_))
    probe.expectMsg(1 second, events(0))
    probe.expectNoMsg(100 milli)

    eventuals ! new EventAck(events(0))
    probe.expectMsg(1 second, events(1))
    ()
  }

  it should "handle backpressure if all-events restarts" in {
    val notify = TestProbe()
    val probe = TestProbe()
    val envSeq = new EnvelopeSeq(2L)
    val impl = new EventualsApiImpl(notify.ref, 2L)
    val eventuals = system.actorOf(Props(new Eventuals(implFunc(impl), cl => Props(new ConsistencyBuilderProbe(probe.ref, cl)), maxPendingEvents = 2, allEventsRetryDelay = 1)))
    val events1 = Vector(BasicEvent("v1"), BasicEvent("v2")).map(envSeq.next)
    val events2 = Vector(BasicEvent("v3"), BasicEvent("v4")).map(envSeq.next)
    notify.fishForMessage(1 second) {
      case ("streaming", _) => true
      case _ => false
    }

    events1.foreach(impl.publisher ! AllEventsPublisher.NextEvent(_))
    probe.expectMsg(1 second, events1(0))
    probe.expectMsg(1 second, events1(1))

    impl.publisher ! AllEventsPublisher.Error
    eventuals ! new EventAck(events1(0))
    notify.fishForMessage(1 second) {
      case ("streaming", _) => true
      case _ => false
    }
    events2.foreach(impl.publisher ! AllEventsPublisher.NextEvent(_))
    probe.expectMsg(1 second, events2(0))
    probe.expectNoMsg(100 milli)

    eventuals ! new EventAck(events1(1))
    probe.expectMsg(1 second, events2(1))
    ()
  }
}
