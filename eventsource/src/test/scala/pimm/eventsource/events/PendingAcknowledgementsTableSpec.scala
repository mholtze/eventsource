package pimm.eventsource.events

import akka.actor.Status
import pimm.eventsource.BaseActorTest

import scala.concurrent.duration._
import scala.collection.immutable

class PendingAcknowledgementsTableSpec extends BaseActorTest {
  case class EventsResults(events: immutable.Seq[Any])

  case class BasicEvent(value: String)

  case class EnvelopeImpl(
    offset: Long,
    event: Any,
    aggregateId: String = "agg-1"
  ) extends PersistentEventEnvelope {
    override def sequenceNr: Long = offset
    override def globalSequenceNr: Long = offset
  }

  "A PendingAcknowledgementsTable" should "support adding single event results" in {
    val pendingAcks = new PendingAcknowledgementsTable(5.seconds.toMillis)
    val events = Vector(BasicEvent("v1"))
    val result = EventsResults(events)
    val seqStart = 1
    pendingAcks.add(new AcknowledgeConsistent("agg-1", seqStart, events.size, result, testActor))

    val entries = pendingAcks.results("agg-1")
    val entry = entries.head
    entries.size should be (1)
    entry.pendingCount should be (1)
    entry.received(seqStart) should be (false)
  }

  it should "support adding multi event results" in {
    val pendingAcks = new PendingAcknowledgementsTable(5.seconds.toMillis)
    val events = Vector(BasicEvent("v1"), BasicEvent("v2"))
    val result = EventsResults(events)
    val seqStart = 1
    pendingAcks.add(new AcknowledgeConsistent("agg-1", seqStart, events.size, result, testActor))

    val entries = pendingAcks.results("agg-1")
    val entry = entries.head
    entries.size should be (1)
    entry.pendingCount should be (2)
    entry.received(seqStart) should be (false)
    entry.received(seqStart+1) should be (false)
  }

  it should "notify and remove single event results when acknowledged" in {
    val pendingAcks = new PendingAcknowledgementsTable(5.seconds.toMillis)
    val events = Vector(BasicEvent("v1"))
    val result = EventsResults(events)
    val seqStart = 1
    pendingAcks.add(new AcknowledgeConsistent("agg-1", seqStart, events.size, result, testActor))
    pendingAcks.receiveEvent("agg-1", seqStart)

    expectMsg(1 second, Status.Success(result))
    pendingAcks.results.get("agg-1") should be (None)
  }

  it should "notify and remove multi event results when acknowledged in order" in {
    val pendingAcks = new PendingAcknowledgementsTable(5.seconds.toMillis)
    val events = Vector(BasicEvent("v1"), BasicEvent("v2"))
    val result = EventsResults(events)
    val seqStart = 1
    pendingAcks.add(new AcknowledgeConsistent("agg-1", seqStart, events.size, result, testActor))

    pendingAcks.receiveEvent("agg-1", seqStart)
    val entry = pendingAcks.results("agg-1").head
    entry.pendingCount should be (1)
    entry.received(seqStart) should be (true)
    entry.received(seqStart+1) should be (false)

    pendingAcks.receiveEvent("agg-1", seqStart+1)
    expectMsg(1 second, Status.Success(result))
    pendingAcks.results.get("agg-1") should be (None)
  }

  it should "notify and remove multi event results when acknowledged out of order" in {
    val pendingAcks = new PendingAcknowledgementsTable(5.seconds.toMillis)
    val events = Vector(BasicEvent("v1"), BasicEvent("v2"))
    val result = EventsResults(events)
    val seqStart = 1
    pendingAcks.add(new AcknowledgeConsistent("agg-1", seqStart, events.size, result, testActor))

    pendingAcks.receiveEvent("agg-1", seqStart+1)
    val entry = pendingAcks.results("agg-1").head
    entry.pendingCount should be (1)
    entry.received(seqStart) should be (false)
    entry.received(seqStart+1) should be (true)

    pendingAcks.receiveEvent("agg-1", seqStart)
    expectMsg(1 second, Status.Success(result))
    pendingAcks.results.get("agg-1") should be (None)
  }

  it should "AckTimeout and remove single event results" in {
    val pendingAcks = new PendingAcknowledgementsTable(5.seconds.toMillis)
    val events = Vector(BasicEvent("v1"))
    val result = EventsResults(events)
    val timeout = AckTimeout(EnvelopeImpl(1, events(0)))
    val seqStart = 1
    pendingAcks.add(new AcknowledgeConsistent("agg-1", seqStart, events.size, result, testActor))

    pendingAcks.receiveTimeout(timeout)
    expectMsgPF(1 second) {
      case Status.Failure(cause: AcknowledgedTimeoutException) =>
        cause.result should be (result)
        cause.timeout should be (Some(timeout))
    }
    pendingAcks.results.get("agg-1") should be (None)
  }

  it should "AckTimeout and remove multi event results" in {
    val pendingAcks = new PendingAcknowledgementsTable(5.seconds.toMillis)
    val events = Vector(BasicEvent("v1"), BasicEvent("v2"))
    val result = EventsResults(events)
    val timeout = AckTimeout(EnvelopeImpl(1, events(0)))
    val seqStart = 1
    pendingAcks.add(new AcknowledgeConsistent("agg-1", seqStart, events.size, result, testActor))

    pendingAcks.receiveEvent("agg-1", seqStart+1)
    pendingAcks.receiveTimeout(timeout)
    expectMsgPF(1 second) {
      case Status.Failure(cause: AcknowledgedTimeoutException) =>
        cause.result should be (result)
        cause.timeout should be (Some(timeout))
    }
    pendingAcks.results.get("agg-1") should be (None)
  }

  it should "support adding events for different aggregates" in {
    val pendingAcks = new PendingAcknowledgementsTable(5.seconds.toMillis)
    val result1 = EventsResults(Vector(BasicEvent("v1")))
    val result2 = EventsResults(Vector(BasicEvent("v2")))
    val seqStart1 = 1
    val seqStart2 = 5
    pendingAcks.add(new AcknowledgeConsistent("agg-1", seqStart1, result1.events.size, result1, testActor))
    pendingAcks.add(new AcknowledgeConsistent("agg-2", seqStart2, result2.events.size, result2, testActor))

    val entries1 = pendingAcks.results("agg-1")
    val entries2 = pendingAcks.results("agg-2")

    entries1.size should be (1)
    entries2.size should be (1)

    entries1.head.pendingCount should be (1)
    entries2.head.pendingCount should be (1)

    entries1.head.received(seqStart1) should be (false)
    entries2.head.received(seqStart2) should be (false)
  }

  it should "acknowledge events for different aggregates" in {
    val pendingAcks = new PendingAcknowledgementsTable(5.seconds.toMillis)
    val result1 = EventsResults(Vector(BasicEvent("v1")))
    val result2 = EventsResults(Vector(BasicEvent("v2")))
    val seqStart1 = 1
    val seqStart2 = 5
    pendingAcks.add(new AcknowledgeConsistent("agg-1", seqStart1, result1.events.size, result1, testActor))
    pendingAcks.add(new AcknowledgeConsistent("agg-2", seqStart2, result2.events.size, result2, testActor))
    pendingAcks.receiveEvent("agg-1", seqStart1)

    expectMsg(1 second, Status.Success(result1))

    val entries1 = pendingAcks.results.get("agg-1")
    val entries2 = pendingAcks.results("agg-2")

    entries1 should be (None)
    entries2.size should be (1)
    entries2.head.received(seqStart2) should be (false)
  }

  it should "support AckTimeouts across different aggregates" in {
    val pendingAcks = new PendingAcknowledgementsTable(5.seconds.toMillis)
    val result1 = EventsResults(Vector(BasicEvent("v1")))
    val result2 = EventsResults(Vector(BasicEvent("v2")))
    val seqStart1 = 1
    val seqStart2 = 5
    pendingAcks.add(new AcknowledgeConsistent("agg-1", seqStart1, result1.events.size, result1, testActor))
    pendingAcks.add(new AcknowledgeConsistent("agg-2", seqStart2, result2.events.size, result2, testActor))

    val timeout = AckTimeout(EnvelopeImpl(1, result1.events(0)))
    pendingAcks.receiveTimeout(timeout)

    expectMsgPF(1 second) {
      case Status.Failure(cause: AcknowledgedTimeoutException) =>
        cause.result should be (result1)
        cause.timeout should be (Some(timeout))
    }

    val entries1 = pendingAcks.results.get("agg-1")
    val entries2 = pendingAcks.results("agg-2")

    entries1 should be (None)
    entries2.size should be (1)
    entries2.head.received(seqStart2) should be (false)
  }

  it should "support adding multiple entries for a single aggregate" in {
    val pendingAcks = new PendingAcknowledgementsTable(5.seconds.toMillis)
    val result1 = EventsResults(Vector(BasicEvent("v1")))
    val result2 = EventsResults(Vector(BasicEvent("v2")))
    val seqStart1 = 1
    val seqStart2 = 5
    pendingAcks.add(new AcknowledgeConsistent("agg-1", seqStart1, result1.events.size, result1, testActor))
    pendingAcks.add(new AcknowledgeConsistent("agg-1", seqStart2, result2.events.size, result2, testActor))

    val entries = pendingAcks.results("agg-1")
    entries.size should be (2)

    val entry1 = entries.find(_.startSequenceNr==seqStart1).get
    val entry2 = entries.find(_.startSequenceNr==seqStart2).get

    entry1.pendingCount should be (1)
    entry2.pendingCount should be (1)

    entry1.received(seqStart1) should be (false)
    entry2.received(seqStart2) should be (false)
  }

  it should "acknowledge events for multiple entries of a single aggregate" in {
    val pendingAcks = new PendingAcknowledgementsTable(5.seconds.toMillis)
    val result1 = EventsResults(Vector(BasicEvent("v1")))
    val result2 = EventsResults(Vector(BasicEvent("v2")))
    val seqStart1 = 1
    val seqStart2 = 5
    pendingAcks.add(new AcknowledgeConsistent("agg-1", seqStart1, result1.events.size, result1, testActor))
    pendingAcks.add(new AcknowledgeConsistent("agg-1", seqStart2, result2.events.size, result2, testActor))
    pendingAcks.receiveEvent("agg-1", seqStart1)

    expectMsg(1 second, Status.Success(result1))

    val entries = pendingAcks.results("agg-1")
    entries.size should be (1)

    val entry2 = entries.find(_.startSequenceNr==seqStart2).get
    entry2.pendingCount should be (1)
    entry2.received(seqStart2) should be (false)
  }

  it should "support AckTimeouts across multiple entries for a single aggregate" in {
    val pendingAcks = new PendingAcknowledgementsTable(5.seconds.toMillis)
    val result1 = EventsResults(Vector(BasicEvent("v1")))
    val result2 = EventsResults(Vector(BasicEvent("v2")))
    val seqStart1 = 1
    val seqStart2 = 5
    pendingAcks.add(new AcknowledgeConsistent("agg-1", seqStart1, result1.events.size, result1, testActor))
    pendingAcks.add(new AcknowledgeConsistent("agg-1", seqStart2, result2.events.size, result2, testActor))

    val timeout = AckTimeout(EnvelopeImpl(1, result1.events(0)))
    pendingAcks.receiveTimeout(timeout)

    expectMsgPF(1 second) {
      case Status.Failure(cause: AcknowledgedTimeoutException) =>
        cause.result should be (result1)
        cause.timeout should be (Some(timeout))
    }

    val entries = pendingAcks.results("agg-1")
    entries.size should be (1)

    val entry2 = entries.find(_.startSequenceNr==seqStart2).get
    entry2.pendingCount should be (1)
    entry2.received(seqStart2) should be (false)
  }

  it should "support direct timeouts" in {
    val pendingAcks = new PendingAcknowledgementsTable(10)
    val result1 = EventsResults(Vector(BasicEvent("v1")))
    val result2 = EventsResults(Vector(BasicEvent("v2")))
    val seqStart1 = 1
    val seqStart2 = 5
    pendingAcks.add(new AcknowledgeConsistent("agg-1", seqStart1, result1.events.size, result1, testActor))
    Thread.sleep(20)
    pendingAcks.add(new AcknowledgeConsistent("agg-1", seqStart2, result2.events.size, result2, testActor))

    pendingAcks.checkTimeouts()

    expectMsgPF(1 second) {
      case Status.Failure(cause: AcknowledgedTimeoutException) =>
        cause.result should be (result1)
        cause.timeout should be (None)
    }

    val entries = pendingAcks.results("agg-1")
    entries.size should be (1)

    val entry2 = entries.find(_.startSequenceNr==seqStart2).get
    entry2.pendingCount should be (1)
    entry2.received(seqStart2) should be (false)
  }
}
