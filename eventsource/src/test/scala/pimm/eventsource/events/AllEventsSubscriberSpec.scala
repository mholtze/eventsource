package pimm.eventsource.events

import akka.actor._
import akka.stream.ActorMaterializer
import akka.stream.actor.ActorPublisher
import akka.stream.scaladsl.{Sink, Source}
import akka.testkit.TestProbe
import pimm.eventsource.BaseActorTest
import pimm.eventsource.events.AllEventsSubscriber.{PendingCount, QueryPending, DecrementPending}
import pimm.eventsource.events.Eventuals.AllEvents
import scala.concurrent.Future
import scala.concurrent.duration._

class AllEventsSubscriberSpec extends BaseActorTest {
  private implicit val materializer = ActorMaterializer()

  case class EnvelopeImpl(
      offset: Long,
      aggregateId: String = "agg-1"
  ) extends PersistentEventEnvelope {
    override def sequenceNr: Long = offset
    override def globalSequenceNr: Long = offset
    override def event: Any = offset.toString
  }

  object AllEventsPublisher {
    object NextEvent
    object Complete
    object Error
  }

  class AllEventsPublisher extends ActorPublisher[PersistentEventEnvelope] {
    import akka.stream.actor.ActorPublisherMessage._

    var buf = Vector.empty[PersistentEventEnvelope]
    var seqNum: Long = 0L

    def receive = {
      case AllEventsPublisher.NextEvent =>
        seqNum += 1L
        val x = EnvelopeImpl(seqNum)
        if (buf.isEmpty && totalDemand > 0)
          onNext(x)
        else {
          buf :+= x
          deliverBuf()
        }
      case AllEventsPublisher.Complete => onComplete()
      case AllEventsPublisher.Error => onError(new RuntimeException("Error Test"))

      case Request(_) => deliverBuf()
      case Cancel => context.stop(self)
    }

    def deliverBuf(): Unit =
      if (totalDemand > 0) {
        val (use, keep) = buf.splitAt(totalDemand.toInt)
        buf = keep
        use foreach onNext
      }
  }

  class EventualsApiImpl(val notifier: ActorRef) extends EventualsApi {
    private var lastSeqNr: Long = 0
    var publisher: ActorRef = _

    override def readLastGlobalSeqNr(): Future[Long] = Future.successful(lastSeqNr)
    override def saveLastGlobalSeqNr(lastSeqNr: Long): Unit = this.lastSeqNr = lastSeqNr
    override def allEventsLive(globalFrom: Long): Source[PersistentEventEnvelope, Unit] = {
      Source
        .actorPublisher[PersistentEventEnvelope](Props(new AllEventsPublisher()))
        .mapMaterializedValue(a => {
          publisher = a
          notifier ! "streaming"
        })
    }
  }

  "An AllEventsSubscriber" should "forward persistent event envelopes to the consumer" in {
    val notify = TestProbe()
    val probe = TestProbe()
    val impl = new EventualsApiImpl(notify.ref)
    impl.allEventsLive(0).runWith(Sink.actorSubscriber(Props(new AllEventsSubscriber(probe.ref, 1000, 0))))
    notify.expectMsg(1 second, "streaming")
    impl.publisher ! AllEventsPublisher.NextEvent
    probe.expectMsgPF(1 second) { case AllEvents(subscriber, _) => true }
    probe.expectMsgClass(1 second, classOf[EnvelopeImpl])
    ()
  }

  it should "send an exception if complete message is sent" in {
    val notify = TestProbe()
    val probe = TestProbe()
    val impl = new EventualsApiImpl(notify.ref)
    impl.allEventsLive(0).runWith(Sink.actorSubscriber(Props(new AllEventsSubscriber(probe.ref, 1000, 0))))

    notify.expectMsg(1 second, "streaming")
    probe.expectMsgPF(1 second) { case AllEvents(subscriber, _) => true }
    impl.publisher ! AllEventsPublisher.NextEvent
    probe.expectMsgClass(1 second, classOf[EnvelopeImpl])
    impl.publisher ! AllEventsPublisher.Complete
    probe.expectMsgPF(1 second) {
      case ex: Eventuals.AllEventsException =>
        if (ex.getMessage != "The all events stream stopped unexpectedly")
          throw new RuntimeException(ex)
    }
    ()
  }

  it should "send an exception if there is a stream failure" in {
    val notify = TestProbe()
    val probe = TestProbe()
    val impl = new EventualsApiImpl(notify.ref)
    impl.allEventsLive(0).runWith(Sink.actorSubscriber(Props(new AllEventsSubscriber(probe.ref, 1000, 0))))

    notify.expectMsg(1 second, "streaming")
    probe.expectMsgPF(1 second) { case AllEvents(subscriber, _) => true }
    impl.publisher ! AllEventsPublisher.NextEvent
    probe.expectMsgClass(1 second, classOf[EnvelopeImpl])
    impl.publisher ! AllEventsPublisher.Error
    probe.expectMsgPF(1 second) {
      case ex: Eventuals.AllEventsException =>
        if (ex.getMessage != "The all events stream failed")
          throw new RuntimeException(ex)
    }
    ()
  }

  it should "handle backpressure" in {
    val notify = TestProbe()
    val probe = TestProbe()
    val impl = new EventualsApiImpl(notify.ref)
    var allEvents: ActorRef = null

    impl.allEventsLive(0).runWith(Sink.actorSubscriber(Props(new AllEventsSubscriber(probe.ref, 1, 0))))
    notify.expectMsg(1 second, "streaming")
    probe.expectMsgPF(1 second) {
      case AllEvents(subscriber, _) =>
        allEvents = subscriber
        true
    }

    impl.publisher ! AllEventsPublisher.NextEvent
    impl.publisher ! AllEventsPublisher.NextEvent
    probe.expectMsgClass(1 second, classOf[EnvelopeImpl])
    allEvents ! QueryPending
    probe.expectMsg(1 second, PendingCount(1))

    probe.expectNoMsg(100 milli)
    allEvents ! DecrementPending(1)
    probe.expectMsgClass(1 second, classOf[EnvelopeImpl])
    allEvents ! QueryPending
    probe.expectMsg(1 second, PendingCount(1))

    allEvents ! DecrementPending(1)
    allEvents ! QueryPending
    probe.expectMsg(1 second, PendingCount(0))
    ()
  }
}
