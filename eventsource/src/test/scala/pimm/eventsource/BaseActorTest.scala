package pimm.eventsource

import akka.actor.ActorSystem
import akka.testkit.TestKit
import org.scalatest.{BeforeAndAfterAll, Matchers, FlatSpecLike}

abstract class BaseActorTest(_system: ActorSystem) extends TestKit(_system)
  with FlatSpecLike with Matchers with BeforeAndAfterAll
{
  def this() = this(ActorSystem("unit-test"))

  override def afterAll: Unit = {
    TestKit.shutdownActorSystem(system)
  }
}


