val releaseV = "1.0.5"

val scalaV = "2.11.7"

val AkkaV = "2.4.0"

name := "eventsource"

version := releaseV

scalaVersion := scalaV

val commonDeps = Seq(
  ("com.typesafe.akka" %% "akka-persistence" % AkkaV % "provided")
    .exclude("org.iq80.leveldb", "leveldb")
    .exclude("org.fusesource.leveldbjni", "leveldbjni-all"),
  ("nl.grons" %% "metrics-scala" % "3.5.1_a2.3" % "provided")
    .exclude("com.typesafe.akka", "akka-actor_2.10")
    .exclude("com.typesafe.akka", "akka-actor_2.11"),
  "com.typesafe.akka" %% "akka-persistence-query-experimental" % AkkaV % "provided",
  "com.typesafe.akka" % "akka-stream-experimental_2.11" % "1.0" % "provided",
  "com.google.guava" % "guava" % "19.0" % "provided",
  "org.slf4j" % "slf4j-simple" % "1.7.12" % "test",
  "org.scalatest" %% "scalatest" % "2.1.7" % "test",
  "junit" % "junit" % "4.11" % "test",
  "org.mockito" % "mockito-all" % "1.9.5" % "test",
  "de.flapdoodle.embed" % "de.flapdoodle.embed.mongo" % "1.48.2" % "test",
  "com.typesafe.akka" %% "akka-testkit" % AkkaV % "test"
)

val commonSettings = Seq(
  scalaVersion := scalaV,
  libraryDependencies ++= commonDeps,
  version := releaseV,
  publishTo := Some(Resolver.file("Procuro Repository Publish", file("R:\\")).mavenStyle().transactional()),
  organization := "com.procuro",
  scalacOptions ++= Seq(
    "-unchecked",
    "-deprecation",
    "-encoding", "UTF-8",       // yes, this is 2 args
    "-language:existentials",
    "-language:higherKinds",
    "-language:implicitConversions",
    // "-Xfatal-warnings",      Deprecations keep from enabling this
    "-Xlint",
    "-Yno-adapted-args",
    "-Ywarn-dead-code",        // N.B. doesn't work well with the ??? hole
    //"-Ywarn-numeric-widen",
    "-Ywarn-value-discard",
    "-Xfuture",
    "-Ywarn-unused-import",     // 2.11 only
    "-target:jvm-1.8"
  ),
  javacOptions ++= Seq(
    "-source", "1.8",
    "-target", "1.8",
    "-Xlint"
  ),
  resolvers ++= Seq(
    "Procuro Repository" at "file:///R:\\",
    "Typesafe Releases" at "http://repo.typesafe.com/typesafe/releases/",
    "Typesafe Snapshots" at "http://repo.typesafe.com/typesafe/snapshots/",
    "Sonatype OSS Snapshots" at "https://oss.sonatype.org/content/repositories/snapshots"
  ),
  parallelExecution in Test := false
)

lazy val `eventsource` = (project in file("eventsource"))
  .settings(commonSettings:_*)
  .settings(
    libraryDependencies ++= Seq(
      "com.typesafe.akka" %% "akka-persistence-tck" % AkkaV % "test"
    )
  )

lazy val `eventsource-mongo` = (project in file("mongo"))
  .dependsOn(`eventsource` % "test->test;compile->compile")
  .settings(commonSettings:_*)
  .settings(
    libraryDependencies ++= Seq(
      ("org.reactivemongo" %% "reactivemongo" % "0.11.6" % "provided")
        .exclude("com.typesafe.akka","akka-actor_2.10")
        .exclude("com.typesafe.akka","akka-actor_2.11"),
      ("com.procuro.scullxbones" %% "akka-persistence-mongo-rxmongo" % "1.0.14" % "provided"),
      "org.mongodb" % "mongo-java-driver" % "2.13.1" % "test",
      "de.flapdoodle.embed" % "de.flapdoodle.embed.mongo" % "1.48.2" % "test"
    )
  )

lazy val `eventsource-root` = (project in file("."))
  .aggregate(`eventsource`, `eventsource-mongo`)
  .settings(commonSettings:_*)
  .settings(
    packagedArtifacts in file(".") := Map.empty)

