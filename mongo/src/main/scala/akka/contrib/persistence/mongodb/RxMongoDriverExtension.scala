package akka.contrib.persistence.mongodb

import akka.actor._
import akka.persistence.Persistence

object RxMongoDriverExtension extends ExtensionId[RxMongoDriverExtension] with ExtensionIdProvider {
  override def lookup = RxMongoDriverExtension
  override def createExtension(system: ExtendedActorSystem) = new RxMongoDriverExtension(system)
  /** Java API: retrieve the Count extension for the given system. */
  override def get(system: ActorSystem): RxMongoDriverExtension = super.get(system)
}

/** Helper to access the akka RxMongoDriver. */
final class RxMongoDriverExtension(system: ActorSystem) extends Extension {
  private lazy val driver = {
    val persistence = Persistence(system)
    val config = persistence.journalConfigFor("akka-contrib-mongodb-persistence-journal")
    val ext = MongoPersistenceExtension(system).asInstanceOf[RxMongoPersistenceExtension]
    ext(config).asInstanceOf[ext.Configured].driver
  }

  def dbName: String = driver.dbName
  def db = driver.db
  def collection(name: String) = driver.collection(name)
}
