package pimm.eventsource.mongodb

import akka.actor._
import akka.contrib.persistence.mongodb._
import akka.persistence.query.PersistenceQuery
import akka.stream.scaladsl.Source
import pimm.eventsource.events.{PersistentEventEnvelope, EventualsApi}
import reactivemongo.api.collections.bson.BSONCollection
import reactivemongo.api.commands.WriteResult
import reactivemongo.bson.BSONDocument

import scala.concurrent.duration._
import scala.concurrent.{ExecutionContext, Promise, Future}
import scala.util.{Failure, Success}

private [mongodb] trait MongoEventualsDB {
  def flushInterval: FiniteDuration
  private [mongodb] def globalSeqNrCollection: BSONCollection
  def readLastGlobalSeqNr(): Future[Long]
  private [mongodb] def updateLastGlobalSeqNr(lastSeqNr: Long): Future[WriteResult]
}

final class MongoEventualsApi(system: ActorSystem, eventuals: ActorRef) extends EventualsApi with MongoEventualsDB {
  import ExecutionContext.Implicits.global
  import RxMongoPersistenceDriver.toWriteConcern
  import MongoEventualsActor.ScheduleSaveLastSeqNr

  private val config = system.settings.config.getConfig("pimm.eventsource.events.eventuals.mongo")

  val globalSeqNrCollectionName = config.getString("global-seqnr-collection")
  val globalSeqNrVersion = config.getInt("global-seqnr-version")
  val flushInterval = config.getDuration("global-seqnr-flush-interval" ,MILLISECONDS).millis

  private val globalSeqNrWriteConcern = toWriteConcern(
    config.getString("global-seqnr-concern"),
    config.getDuration("global-seqnr-wtimeout" ,MILLISECONDS).millis,
    config.getBoolean("global-seqnr-fsync")
  )

  private val driverExtension = RxMongoDriverExtension(system)
  private val readJournal = PersistenceQuery(system).readJournalFor[ScalaDslMongoReadJournal](MongoReadJournal.Identifier)
  private val updater = system.actorOf(Props(new MongoEventualsActor(this)))

  private [mongodb] override def globalSeqNrCollection = driverExtension.collection(globalSeqNrCollectionName)

  // todo document
  /*
  { _id: version, gsn: Long }
   */

  override def readLastGlobalSeqNr(): Future[Long] = {
    val query = BSONDocument("_id" -> globalSeqNrVersion)
    val fields = BSONDocument("gsn" -> 1)
    val promise = Promise[Long]()

    globalSeqNrCollection
      .find(query, fields)
      .one[BSONDocument]
      .onComplete {
        case Success(value) => value match {
          case Some(doc) => promise.success(doc.getAs[Long]("gsn").get)
          case None => insertAndReturnZero(promise)
        }
        case Failure(cause) => promise.failure(cause)
      }
    promise.future
  }

  private def insertAndReturnZero(promise: Promise[Long]): Unit = {
    globalSeqNrCollection.insert(BSONDocument("_id" -> globalSeqNrVersion, "gsn" -> 0L))
      .onComplete {
        case Success(wr) =>
          if (wr.ok) promise.success(0L)
          else throw wr
        case Failure(cause) => promise.failure(cause)
      }
  }

  override def saveLastGlobalSeqNr(lastSeqNr: Long): Unit = {
    updater ! ScheduleSaveLastSeqNr(lastSeqNr)
  }

  private [mongodb] override def updateLastGlobalSeqNr(lastSeqNr: Long): Future[WriteResult] = {
    val query = BSONDocument("_id" -> globalSeqNrVersion)
    val update = BSONDocument("$set" -> BSONDocument("gsn" -> lastSeqNr))
    globalSeqNrCollection.update(query, update, writeConcern = globalSeqNrWriteConcern)
  }

  override def allEventsLive(globalFrom: Long): Source[PersistentEventEnvelope, Unit] =
    readJournal.liveGlobalEvents(Some(globalFrom)).map(new MongoEventEnvelope(_))
}
