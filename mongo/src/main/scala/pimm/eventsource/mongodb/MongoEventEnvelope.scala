package pimm.eventsource.mongodb

import akka.contrib.persistence.mongodb.GlobalEventEnvelope
import pimm.eventsource.events.PersistentEventEnvelope


final class MongoEventEnvelope(globalEnvelope: GlobalEventEnvelope) extends PersistentEventEnvelope{
  override def aggregateId: String = globalEnvelope.persistenceId
  override def globalSequenceNr: Long = globalEnvelope.globalSequenceNr
  override def event: Any = globalEnvelope.event
  override def sequenceNr: Long = globalEnvelope.sequenceNr
}
