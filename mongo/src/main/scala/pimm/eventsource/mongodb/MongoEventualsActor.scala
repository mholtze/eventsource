package pimm.eventsource.mongodb

import akka.actor.Actor
import akka.event.Logging

import scala.concurrent.Await
import scala.util.{Failure, Success}
import scala.concurrent.duration._


private [mongodb] object MongoEventualsActor {
  case object SaveLastSeqNumber
  case class ScheduleSaveLastSeqNr(lastSeqNr: Long)
  private case class SeqNumberUpdated(savedSeqNr: Long)
  private case class SeqNumberUpdateFailed(cause: Throwable, seqNrToSave: Long)
}

private [mongodb] final class MongoEventualsActor(val impl: MongoEventualsDB) extends Actor {
  import MongoEventualsActor._
  import context.dispatcher

  val log = Logging(context.system, this)
  var savedGlobalSeqNr = 0L
  var lastGlobalSeqNr = 0L
  var hasScheduledUpdate = false

  override def receive = {
    case ScheduleSaveLastSeqNr(lastSeqNr) => if (lastSeqNr > lastGlobalSeqNr) scheduleUpdate(lastSeqNr)
    case SaveLastSeqNumber => if (lastGlobalSeqNr > savedGlobalSeqNr) updateLastSeqNumber()
    case SeqNumberUpdated(savedSeqNr) => if (savedSeqNr > savedGlobalSeqNr) savedGlobalSeqNr = savedSeqNr
    case SeqNumberUpdateFailed(cause, seqNrToSave) => handleFailedSeqNumberUpdate(cause, seqNrToSave)
  }

  @throws[Exception](classOf[Exception])
  override def postStop(): Unit = {
    // try to save on stop
    if (lastGlobalSeqNr > savedGlobalSeqNr) {
      try {
        val result = Await.result(impl.updateLastGlobalSeqNr(lastGlobalSeqNr), 1.second)
        if (!result.ok)
          throw result
        else
          savedGlobalSeqNr = lastGlobalSeqNr
      }
      catch {
        case t: Throwable =>
          log.error(t, "Could not update lastGlobalSeqNr in postStop to {}", lastGlobalSeqNr)
      }
    }
  }

  def scheduleUpdate(lastSeqNr: Long): Unit = {
    lastGlobalSeqNr = lastSeqNr
    if (!hasScheduledUpdate) {
      context.system.scheduler.scheduleOnce(impl.flushInterval, self, SaveLastSeqNumber)
      ()
    }
  }

  def updateLastSeqNumber(): Unit = {
    val seqNrToSave = lastGlobalSeqNr
    impl.updateLastGlobalSeqNr(seqNrToSave)
      .onComplete {
        case Success(wr) if wr.ok => self ! SeqNumberUpdated(seqNrToSave)
        case Success(wr) => self ! SeqNumberUpdateFailed(wr, seqNrToSave)
        case Failure(cause) => self ! SeqNumberUpdateFailed(cause, seqNrToSave)
      }
  }

  def handleFailedSeqNumberUpdate(cause: Throwable, seqNrToSave: Long): Unit = {
    log.error(cause, "Could not update lastGlobalSeqNr to {}", seqNrToSave)
    // retry if nothing scheduled
    if (!hasScheduledUpdate && lastGlobalSeqNr > savedGlobalSeqNr) {
      context.system.scheduler.scheduleOnce(impl.flushInterval, self, SaveLastSeqNumber)
      ()
    }
  }
}