package pimm.eventsource.mongodb

import akka.testkit.TestProbe
import reactivemongo.bson.BSONDocument

import scala.concurrent.{ExecutionContext, Await, Future}
import scala.concurrent.duration.FiniteDuration
import scala.concurrent.duration._

class MongoEventualsDBSpec extends BaseMongoTest {
  import ExecutionContext.Implicits.global

  def timeoutDur: FiniteDuration = 1 second

  def getGlobalSeqNrDoc(api: MongoEventualsApi): Future[Option[BSONDocument]] = {
    val query = BSONDocument("_id" -> api.globalSeqNrVersion)
    api.globalSeqNrCollection.find(query).one[BSONDocument]
  }

  "The MongoEventuals DB API" should "auto create the global sequence number document" in withSystem { (system, eventSystem) =>
    val eventuals = TestProbe()(system)
    val api = new MongoEventualsApi(system, eventuals.ref)
    val globalSeqNr = Await.result(api.readLastGlobalSeqNr(), timeoutDur)
    val doc = Await.result(getGlobalSeqNrDoc(api), timeoutDur).get

    globalSeqNr should be (0)
    doc.getAs[Int]("_id").get should be (api.globalSeqNrVersion)
    doc.getAs[Long]("gsn").get should be (0L)
  }

  it should "update with a new sequence number" in withSystem { (system, eventSystem) =>
    val eventuals = TestProbe()(system)
    val api = new MongoEventualsApi(system, eventuals.ref)
    Await.result(api.readLastGlobalSeqNr(), timeoutDur) // ensure the document exists
    Await.result(api.updateLastGlobalSeqNr(10L), timeoutDur) // update

    val doc = Await.result(getGlobalSeqNrDoc(api), timeoutDur).get
    doc.getAs[Int]("_id").get should be (api.globalSeqNrVersion)
    doc.getAs[Long]("gsn").get should be (10L)
  }

  it should "read the last saved sequence number" in withSystem { (system, eventSystem) =>
    val eventuals = TestProbe()(system)
    val api = new MongoEventualsApi(system, eventuals.ref)
    Await.result(api.readLastGlobalSeqNr(), timeoutDur) // ensure the document exists
    Await.result(api.updateLastGlobalSeqNr(15L), timeoutDur) // update

    val lastSeqNr = Await.result(api.readLastGlobalSeqNr(), timeoutDur)
    lastSeqNr should be (15L)
  }
}
