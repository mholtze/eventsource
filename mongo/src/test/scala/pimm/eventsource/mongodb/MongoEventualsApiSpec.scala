package pimm.eventsource.mongodb

import akka.actor.ActorSystem
import akka.contrib.persistence.mongodb.{ConfiguredExtension, MongoPersistenceExtension, RxMongoDriverExtension}
import com.typesafe.config.Config
import org.scalatest.concurrent.Eventually
import org.scalatest.time._
import pimm.eventsource.aggregate.{CommandReply, CommandRequest}
import reactivemongo.api.commands.UpdateWriteResult
import reactivemongo.bson.BSONDocument

import scala.concurrent.{Await, Future, ExecutionContext}
import scala.concurrent.duration.FiniteDuration
import scala.concurrent.duration._
import scala.collection.mutable

class MongoEventualsApiSpec extends BaseMongoTest with Eventually {
  import BaseMongoTest._
  import ExecutionContext.Implicits.global

  override def timeoutDur: FiniteDuration = 5 second
  def timeoutSpan = timeout(Span(timeoutDur.toMillis, Millis))

  def globalSeqNrCollection(system: ActorSystem) = {
    val driverExtension = RxMongoDriverExtension(system)
    driverExtension.collection("eventsource_global_seqnr")
  }

  def getGlobalSeqNrDoc(system: ActorSystem): Future[Option[BSONDocument]] = {
    val query = BSONDocument("_id" -> 1)
    globalSeqNrCollection(system).find(query).one[BSONDocument]
  }

  def updateLastGlobalSeqNr(system: ActorSystem, lastSeqNr: Long): Future[UpdateWriteResult] = {
    val query = BSONDocument("_id" -> 1)
    val update = BSONDocument("$set" -> BSONDocument("gsn" -> lastSeqNr))
    globalSeqNrCollection(system).update(query, update)
  }

  "The MongoEventualsApi" should "send events to the eventuals loop" in withSystem(configWithEventProbe) { (system, eventSystem) =>
    val probe = ProbingConsistencyBuilder(system).probe
    eventSystem.to(StringAggregate, "agg-1") ! AppendCommand("r2")
    probe.expectMsg(timeoutDur, Append("r2"))
  }

  it should "save the global seq number" in withSystem(configWithEnvelopeProbe) { (system, eventSystem) =>
    val probe = ProbingConsistencyBuilder(system).probe
    eventSystem.to(StringAggregate, "agg-2") ! AppendCommand("r3")

    val lastEnvelope = probe.fishForMessage(timeoutDur) {
      case env: MongoEventEnvelope if env.event == Append("r3") => true
    }.asInstanceOf[MongoEventEnvelope]

    eventually (timeoutSpan, interval(Span(50, Millis))) {
      val doc = Await.result(getGlobalSeqNrDoc(system), timeoutDur).get
      doc.getAs[Long]("gsn").get should be (lastEnvelope.globalSequenceNr)
    }
  }

  it should "restart at last save global sequence nuber and replay any events not saved" in {
    var lastSavedGlobalSeqNr = 0L
    // first step is to save an event and then reset the last global seq number
    withSystem(configWithEnvelopeProbe) { (system, eventSystem) =>
      val probe = ProbingConsistencyBuilder(system).probe
      eventSystem.to(StringAggregate, "agg-3") ! AppendCommand("r4")
      eventSystem.to(StringAggregate, "agg-3") ! AppendCommand("r5")

      val lastEnvelope = probe.fishForMessage(timeoutDur) {
        case env: MongoEventEnvelope if env.event == Append("r4") => false
        case env: MongoEventEnvelope if env.event == Append("r5") => true
      }.asInstanceOf[MongoEventEnvelope]

      lastSavedGlobalSeqNr = lastEnvelope.globalSequenceNr
      eventually (timeoutSpan, interval(Span(50, Millis))) {
        Await.result(getGlobalSeqNrDoc(system), timeoutDur).get.getAs[Long]("gsn").get should be (lastSavedGlobalSeqNr)
      }
      //reset the last global seq number
      Await.result(updateLastGlobalSeqNr(system, lastSavedGlobalSeqNr-1), timeoutDur)
    }
    // verify that the last event is replayed and the global sequence number is updated
    withSystem(configWithEnvelopeProbe) { (system, eventSystem) =>
      val probe = ProbingConsistencyBuilder(system).probe
      probe.fishForMessage(timeoutDur) {
        case env: MongoEventEnvelope if env.event == Append("r5") => true
      }
      eventually (timeoutSpan, interval(Span(50, Millis))) {
        Await.result(getGlobalSeqNrDoc(system), timeoutDur).get.getAs[Long]("gsn").get should be (lastSavedGlobalSeqNr)
      }
    }
  }

  /** This test verifies that that [[RxMongoDriverExtension]] shares its driver with
    * [[akka.contrib.persistence.mongodb.MongoJournal]]
    */
  it should "only have 1 MongoDB journal" in withSystem(configWithEnvelopeProbe) { (system, eventSystem) =>
    // first just execute a simple test that could allocated multiple drivers
    val probe = ProbingConsistencyBuilder(system).probe
    eventSystem.to(StringAggregate, "agg-2") ! AppendCommand("r3")

    val lastEnvelope = probe.fishForMessage(timeoutDur) {
      case env: MongoEventEnvelope if env.event == Append("r3") => true
    }.asInstanceOf[MongoEventEnvelope]

    eventually (timeoutSpan, interval(Span(50, Millis))) {
      val doc = Await.result(getGlobalSeqNrDoc(system), timeoutDur).get
      doc.getAs[Long]("gsn").get should be (lastEnvelope.globalSequenceNr)
    }
    // check that there is only one MongoDB journal
    val ext = MongoPersistenceExtension(system)
    val field = ext.getClass.getDeclaredFields.find(_.getName.endsWith("configuredExtensions")).get
    field.setAccessible(true)
    val configuredExtensions = field.get(ext).asInstanceOf[mutable.Map[Config, ConfiguredExtension]]
    val keys = configuredExtensions.keys.filter(_.getString("class") == "akka.contrib.persistence.mongodb.MongoJournal")
    keys.size should be (1)
  }
}
