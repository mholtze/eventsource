package pimm.eventsource.mongodb

import akka.actor.{Props, ActorSystem}
import akka.util.Timeout
import com.typesafe.config.{Config, ConfigFactory}

import com.mongodb._
import de.flapdoodle.embed.mongo.{MongodStarter, Command}
import de.flapdoodle.embed.mongo.config._
import de.flapdoodle.embed.mongo.distribution.{Version, IFeatureAwareVersion}
import de.flapdoodle.embed.process.config.IRuntimeConfig
import de.flapdoodle.embed.process.extract.UUIDTempNaming
import de.flapdoodle.embed.process.io.directories.PlatformTempDir
import de.flapdoodle.embed.process.runtime.Network
import org.scalatest.{BeforeAndAfterAll, Matchers, FlatSpecLike}
import pimm.eventsource.EventSourceSystem

import scala.concurrent.duration.FiniteDuration

object BaseMongoTest {
  import pimm.eventsource.aggregate.{AggregateProps, DirectStateAggregate}

  class StringAggregate(val aggregateId: String) extends DirectStateAggregate[String] {
    var state: String = "r1"

    def applyEvent(state: String, seqNr: Long) = {
      case Append(value) => state match {
        case null => value
        case _ => state + " " + value
      }
    }

    def executeCommand = {
      case AppendCommand(values) => commandResult ++ values.map(Append(_))
    }
  }

  object StringAggregate extends AggregateProps[String] {
    override def props(aggregateId: String): Props = Props(new StringAggregate(aggregateId))
  }

  case class Append(value: String)
  case class AppendCommand(values: Seq[String])

  object AppendCommand {
    def apply(value: String): AppendCommand = AppendCommand(Seq(value))
  }
}

abstract class BaseMongoTest extends FlatSpecLike with Matchers with BeforeAndAfterAll {
  def embedConnectionURL: String = { "localhost" }
  lazy val embedConnectionPort: Int = { Network.getFreeServerPort }
  def embedDB: String = "test"

  def envMongoVersion = Option(System.getenv("MONGODB_VERSION")).orElse(Option("3.0"))
  def overrideOptions: MongoCmdOptionsBuilder => MongoCmdOptionsBuilder = useWiredTigerOn30

  def useWiredTigerOn30(builder: MongoCmdOptionsBuilder): MongoCmdOptionsBuilder =
    envMongoVersion.filter(_ == "3.0").map(_ => builder.useStorageEngine("wiredTiger")).getOrElse(builder)

  def determineVersion: IFeatureAwareVersion =
    envMongoVersion.collect {
      case "2.4" => Version.Main.V2_4
      case "2.6" => Version.Main.V2_6
      case "3.0" => Version.Main.V3_0
    }.getOrElse(Version.Main.PRODUCTION)

  val artifactStorePath = new PlatformTempDir()
  val executableNaming = new UUIDTempNaming()
  val command = Command.MongoD
  val runtimeConfig: IRuntimeConfig  = new RuntimeConfigBuilder()
    .defaults(command)
    .artifactStore(new ArtifactStoreBuilder()
      .defaults(command)
      .download(new DownloadConfigBuilder()
        .defaultsForCommand(command)
        .artifactStorePath(artifactStorePath)
      ).executableNaming(executableNaming)
    ).build()

  val mongodConfig = new MongodConfigBuilder()
    .version(determineVersion)
    .cmdOptions(
      overrideOptions(new MongoCmdOptionsBuilder()
        .syncDelay(1)
        .useNoJournal(false)
        .useNoPrealloc(true)
        .useSmallFiles(true)
        .verbose(false)
      ).build()
    )
    .net(new Net("127.0.0.1",embedConnectionPort, Network.localhostIsIPv6()))
    .build()

  lazy val runtime = MongodStarter.getInstance(runtimeConfig)
  lazy val mongod = runtime.prepare(mongodConfig)
  lazy val mongodExe = mongod.start()

  lazy val mongoClient = new MongoClient(embedConnectionURL,embedConnectionPort)

  override def beforeAll(): Unit = {
    mongodExe
    ()
  }

  override def afterAll(): Unit = {
    mongod.stop()
  }

  def config = ConfigFactory.parseString(s"""
    |akka.loggers = ["akka.testkit.TestEventListener"]
    |akka.contrib.persistence.mongodb.mongo.driver = "akka.contrib.persistence.mongodb.RxMongoPersistenceExtension"
    |akka.contrib.persistence.mongodb.mongo.journal-read-fill-limit = 10
    |akka.contrib.persistence.mongodb.mongo.mongouri = "mongodb://localhost:$embedConnectionPort/$embedDB"
    |akka.persistence.journal.plugin = "akka-contrib-mongodb-persistence-journal"
    |akka-contrib-mongodb-persistence-journal {
    |    # Class name of the plugin.
    |  class = "akka.contrib.persistence.mongodb.MongoJournal"
    |}
    |akka.persistence.snapshot-store.plugin = "akka-contrib-mongodb-persistence-snapshot"
    |akka-contrib-mongodb-persistence-snapshot {
    |    # Class name of the plugin.
    |  class = "akka.contrib.persistence.mongodb.MongoSnapshots"
    |}
    |akka-contrib-mongodb-persistence-readjournal {
    |  # Class name of the plugin.
    |  class = "akka.contrib.persistence.mongodb.MongoReadJournal"
    |}
    |pimm.eventsource.events.eventuals.api-class = "pimm.eventsource.mongodb.MongoEventualsApi"
    |pimm.eventsource.events.eventuals.mongo.global-seqnr-flush-interval = 50ms
    |""".stripMargin).withFallback(ConfigFactory.defaultReference())


  def configWithEventProbe = ConfigFactory.parseString("""
     |pimm.eventsource.events.eventuals.consistency-builder-class = "pimm.eventsource.mongodb.EventProbeConsistencyBuilder"
     |""".stripMargin).withFallback(config)

  def configWithEnvelopeProbe = ConfigFactory.parseString("""
     |pimm.eventsource.events.eventuals.consistency-builder-class = "pimm.eventsource.mongodb.EnvelopeProbeConsistencyBuilder"
     |""".stripMargin).withFallback(config)

  def timeoutDur: FiniteDuration
  implicit val futureTimeout = Timeout(timeoutDur)

  def withSystem[T](config: Config)(testCode: (ActorSystem, EventSourceSystem) => Any): Unit = {
    val system: ActorSystem = ActorSystem("unit-test", config)
    val eventSystem = EventSourceSystem(system)
    try {
      testCode(system, eventSystem)
      ()
    } finally {
      system.terminate()
      ()
    }
  }

  def withSystem[T](testCode: (ActorSystem, EventSourceSystem) => Any): Unit = withSystem(config)(testCode)

}
