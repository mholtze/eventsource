package pimm.eventsource.mongodb

import akka.actor.{Props, ActorSystem}
import akka.testkit.{TestActorRef, TestProbe, TestKit}
import org.scalatest.concurrent.Eventually
import org.scalatest.time.{Millis, Span}
import org.scalatest.{BeforeAndAfterAll, Matchers, FlatSpecLike}
import reactivemongo.api.collections.bson.BSONCollection
import reactivemongo.api.commands.{DefaultWriteResult, WriteResult}

import scala.concurrent.{Await, Promise, Future}
import scala.concurrent.duration._

class MongoEventualsActorSpec(_system: ActorSystem) extends TestKit(_system)
  with FlatSpecLike with Matchers with BeforeAndAfterAll with Eventually
{
  import MongoEventualsActor._

  def this() = this(ActorSystem("unit-test"))

  def timeoutDur: FiniteDuration = 1 second
  def timeoutSpan = timeout(Span(timeoutDur.toMillis, Millis))

  override def afterAll: Unit = {
    TestKit.shutdownActorSystem(system)
  }

  class DBImpl(
    val flushInterval: FiniteDuration = 50.milli
  ) extends MongoEventualsDB {

    private var _lastSeqNr = 0L
    private var _updateAttempts = 0
    private var _updateEnabled = true

    def lastSeqNr = synchronized { _lastSeqNr }
    def updateAttempts = synchronized { _updateAttempts }

    def updateEnabled = synchronized { _updateEnabled }
    def updateEnabled_=(enabled:Boolean): Unit = synchronized { _updateEnabled = enabled }

    override private [mongodb] def updateLastGlobalSeqNr(lastSeqNr: Long): Future[WriteResult] = synchronized {
      _updateAttempts += 1
      assert(_updateEnabled)
      _lastSeqNr = lastSeqNr
      Promise.successful(DefaultWriteResult(true, 1, Seq(), None, None, None)).future
    }

    override def readLastGlobalSeqNr(): Future[Long] = synchronized {
      Promise.successful(_lastSeqNr).future
    }

    override private [mongodb] def globalSeqNrCollection: BSONCollection = ???
  }


  "The MongoEventualsActor" should "schedule to save the last sequence number" in {
    val db = new DBImpl()
    val actor = system.actorOf(Props(new MongoEventualsActor(db)))
    actor ! ScheduleSaveLastSeqNr(5L)
    db.lastSeqNr should be (0L)

    eventually (timeoutSpan, interval(Span(50, Millis))) {
      db.lastSeqNr should be (5L)
    }
    db.updateAttempts should be (1)
  }

  it should "retry save if failed" in {
    val db = new DBImpl()
    val actor = system.actorOf(Props(new MongoEventualsActor(db)))
    db.updateEnabled = false
    actor ! ScheduleSaveLastSeqNr(5L)
    db.lastSeqNr should be (0L)

    eventually (timeoutSpan, interval(Span(50, Millis))) {
      db.updateAttempts should be (2L)
    }
    db.lastSeqNr should be (0L)
    db.updateAttempts should be > 1
  }

  it should "save on stop" in {
    val db = new DBImpl(flushInterval = 1.minute)
    val actor = system.actorOf(Props(new MongoEventualsActor(db)))
    actor ! ScheduleSaveLastSeqNr(5L)
    db.lastSeqNr should be (0L)

    val deathWatch = TestProbe()
    deathWatch watch(actor)
    system.stop(actor)
    deathWatch.expectTerminated(actor, timeoutDur)

    db.lastSeqNr should be (5L)
    db.updateAttempts should be (1)
  }

  it should "update savedGlobalSeqNr after save" in {
    val db = new DBImpl()
    val actor = TestActorRef[MongoEventualsActor](Props(new MongoEventualsActor(db)))
    actor ! ScheduleSaveLastSeqNr(5L)
    eventually (timeoutSpan, interval(Span(50, Millis))) {
      db.lastSeqNr should be (5L)
    }
    actor.underlyingActor.savedGlobalSeqNr should be (5L)
  }

  it should "ignore unorderd seq numbers" in {
    val db = new DBImpl()
    val actor = TestActorRef[MongoEventualsActor](Props(new MongoEventualsActor(db)))
    actor ! ScheduleSaveLastSeqNr(5L)
    eventually (timeoutSpan, interval(Span(50, Millis))) {
      db.lastSeqNr should be (5L)
    }

    actor.underlyingActor.hasScheduledUpdate should be (false)
    actor ! ScheduleSaveLastSeqNr(4L)
    actor.underlyingActor.hasScheduledUpdate should be (false)
  }

  it should "handle distinct saves" in {
    val db = new DBImpl()
    val actor = TestActorRef[MongoEventualsActor](Props(new MongoEventualsActor(db)))

    actor ! ScheduleSaveLastSeqNr(5L)
    eventually (timeoutSpan, interval(Span(50, Millis))) {
      db.lastSeqNr should be (5L)
    }

    actor ! ScheduleSaveLastSeqNr(6L)
    eventually (timeoutSpan, interval(Span(50, Millis))) {
      db.lastSeqNr should be (6L)
    }

    actor.underlyingActor.savedGlobalSeqNr should be (6L)
  }

  it should "handle overlapping saves" in {
    val db = new DBImpl()
    val actor = TestActorRef[MongoEventualsActor](Props(new MongoEventualsActor(db)))

    actor ! ScheduleSaveLastSeqNr(5L)
    actor ! ScheduleSaveLastSeqNr(6L)
    eventually (timeoutSpan, interval(Span(50, Millis))) {
      db.lastSeqNr should be (6L)
    }

    actor.underlyingActor.savedGlobalSeqNr should be (6L)
    db.updateAttempts should be (1)
  }
}