# Akka Persistence Event Sourcing Extensions

This library builds on top of Akka Persistence adding features that make it much easier to bootstrap event sourced 
programs. The features added include automatic routing and management of aggregate root actors, automated
snapshot strategies, built-in support for in-process view projection, and a 100% in-memory storage plugin
for testing.

## Aggregate Roots

Aggregate roots are one of the fundamental building blocks of event sourcing systems, but only have minimal support
in Akka Persistence. The Event Sourcing Extensions adds a base implementation for aggregate actors. This includes 
routing, aggregate actors, routing and aggregate aware ActorRefs, and strong notion of commands and events.

### Aggregate Routing

To manage aggregate roots and consistently direct messages to the same actor based on aggregate ID and aggregate
type, a special routing actor is used. The routing actor manages a configurable cache of aggregate 
actors. The cache is used to route messages to the same actor each time for a given aggregate. If the aggregate 
is not in the cache the router creates a new instance of the aggregate actor and adds it to the cache.

The aggregate cache tries to keep aggregate roots in memory but can invalidate aggregate actors because of size 
pressure or an expiration policy. The default implementation of the cache uses the 
[Google Guava library cache](https://github.com/google/guava/wiki/CachesExplained) and supports aggregate invalidation
by LRU for cache size and expiration timeouts.

To make is easy to route messages to an aggregate there is simple API to create a proxy to the router which knows
about the target aggregate. This proxy can be used to send messages to the aggregate actor just like a regular
ActorRef, but sends all messages through the aggregate router.
 
    // Event Sourcing ActorSystem extension
    val eventSystem = EventSourceSystem(actorSystem)
    
    // An aggregate ref proxy that can send messages to the aggregate
    val aggregateRef = eventSystem.to(MyAggregate, "myID")
    
    // Send a message to the aggregate is just the same as using a normal ActorRef
    aggregateRef ! "my message"


### AggregateRef

An AggregateRef is a proxy to the router which knows about a target aggregate. As described in the previous section
the AggregateRef makes it easier to send messages to an aggregate actor. On top of this, an AggregateRef also includes 
helper methods to simplify some regular tasks such as getting the current root or waiting to for an update to complete.

Some of the helpers included in AggregateRef.

* **root:** Returns a Future to provide the root of the aggregate.
* **rootOption:** Returns a Future to provide the root of the aggregate as an Option.
* **ask or ?:** Sends a reply after the a message as been processed. If the command changes the root, the reply comes after the events have been persisted.
* **askUnboxed:** Similar to root but can be used with an arbitrary command. If the command changes the root, the reply comes after the events have been persisted.
* **askOption:** Similar to rootOption but can be used with an arbitrary command. If the command changes the root, the reply comes after the events have been persisted.
* **askConsistent:** Sends a reply only after the events have been processed in the Eventuals Loop. This is useful if the caller needs to ensure eventually consistent state like a view projection is updated before performing a task. 

### Aggregate Actors

All aggregate roots are represented by actors, however, implementing the aggregate actors from scratch can become very
complex quickly. To make it easier to implement aggregate actors there is a base AggregateActor class that implements
the basics and some extras necessary for aggregate roots. The AggregateActor base class enables all the features of
the AggregateRef proxy, supports automatic snapshots, and supports commands and events (see section "Commands and
Events" below).

The AggregateActor has a concept of an aggregate root entity. The aggregate root entity is the public interface to
the aggregate which can be retrieved from the aggregate actor for external use. In addition to the aggregate root 
entity the AggregateActor also has a state object. The state object is the state built from events and the data 
saved in snapshots. The root entity is just a wrapper around the state exposing the public functionality of the
aggregate. In addition to encapsulating the state, the root entity can also implement an API for business logic
to interact with the aggregate. If the root entity does implement business logic, all calls to the AggregateActor
must be made through an AggregateRef for proper routing. AggregateActor provides the AggregateRef with
the field "aggregateSelf".

An aggregate may not need a root entity that is different from the aggregate state object. An example would be
state composed of immutable objects and all business logic is invoked by sending messages directly to the actor.
This type of implementation is idiomatic for Akka. As a convenience, a base class called DirectStateAggregate can 
be used for aggregate where the root entity and state object are the same.

### Commands and Events

The Event Sourcing Extensions has a strong notion of commands and events. Commands are sent to an aggregate root to 
perform some action. Changes generated by the command are converted to events which are persisted and applied
to the aggregate root. To guarantee consistency, events only affect the root if the PersistentActor successfully saves 
them. Commands and events are just regular case class objects. Events and commands each have their own dedicated
receive handlers, "executeCommand" and "applyEvent" respectively. 

Below is an example of a simple aggregate containing a string which has commands and events for appending to the
root string.
    
    class StringAggregate(val aggregateId: String) extends DirectStateAggregate[String] {
      var state: String = ""
    
      // Execute the command and return an event
      def executeCommand = {
        case AppendCommand(value) => Appended(value)
      }
    
      // Apply the Append event to the state 
      def applyEvent(state: String) = {
        case Appended(value) => state + value
      }
    }
    
    // Actor Props allocator used by the AggregateRef
    object StringAggregate extends AggregateProps[String] {
      override def props(aggregateId: String): Props = Props(new StringAggregate(aggregateId))
    }
    
    // Event for the appended value
    case class Appended(value: String)
    
    // Command to append a value
    case class AppendCommand(value: String)

    // Send the command to the aggregate
    eventSystem.to(StringAggregate, "myID") ! AppendCommand("a string")

### Command Results and Result Builders

TODO

## Snapshot Strategies

Snapshots make reloading aggregates faster, but snapshots are a manual process in Akka Persistence. The AggregateActor
supports automatic snapshots. The snapshot strategy is configurable and can be set globally in the actor system 
configuration or in an AggregateActor by overriding the "snapshotStrategy" value of the actor. Currently, only one 
strategy called MaxEventsSnapshotStrategy is available. This strategy is simple and saves a snapshot after a 
specified number of events have been received. 

Snapshot strategies also support retrying the snapshot if it fails. Retrying is handled either after a snapshot
failure or when the aggregate actor is restarted. If saving the snapshot fails, the snapshot strategy dictates the
retry policy which includes the number of attempts and any delay. If an actor is restarted before a snapshot could
be saved, a new snapshot attempt will be scheduled after the actor has recovered.
 
Example of setting a custom snapshot strategy:

    class MyAggregate(val aggregateId: String) extends AggregateActor[String] {
      var root = "root"
      override val snapshotStrategy = new MaxEventsSnapshotStrategy(
        maxEvents = 5,
        retryAttempts = 10,
        retryDelay = 10.seconds
      )
    }

## Eventual Consistency and In-process View Projection

A well known complexity with event sourced systems is dealing with eventual consistency for requirements like 
materializing view projections. Often this is done using a message bus of some type and an external service to manage 
the projected view. However, when bootstrapping a new project,creating a prototype, or creating a small-scale system 
(i.e. most systems), the complexity of a message bus and extra services can present a very high barrier to entry and 
discourage many from using event sourcing. To address this issue, the Event Sourcing Extensions has a 
built-in mechanism for handling eventual consistency and view projections. 
 
The in-process mechanism is called the Eventuals Loop. Its mechanics are fairly straightforward. First, each event is 
assigned a global sequence number when it is persisted. After events are saved they are ingested into the Eventuals Loop
in order by their global sequence number. Each event is forwarded to an actor called the Consistency Builder responsible 
for doing eventually consistent operations and projections. After the Consistency Builder has processed an event it
sends an acknowledgement back to the Eventuals Loop. The Eventuals Loop then saves the last processed global 
sequence number.
 
The Eventuals Loop is reliable and guarantees at-least-once delivery for all events. When the Eventuals Loop is restarted
it loads all events since the last global sequence number it saved and replays them. The Consistency Builder must be able
to handle repeated events. Also, the Eventuals Loop can handle unordered acknowledgments from the Consistency Builder
allowing the Consistency Builder to process multiple events concurrently.

## In-Memory Storage Plugin

The Event Sourcing Extensions includes a complete in-memory Akka Persistence Plugin that can be used in testing.
This plugin supports extra features, such as failure simulation, not available with the in-memory plugin bundled with 
Akka Persistence. Also, this in-memory plugin maintains all events and snapshots in memory without serialization.

## Akka Persistence Plugins

The Event Sourcing Extensions are not 100% compatible with all Akka Persistence Plugins. Most critically the plugins
often do not implement global sequence numbers across all events or have live streaming of events. Notable 
exceptions are [Event Store](https://geteventstore.com/) and the MongoDB plugin fork developed in-tandem with 
the The Event Sourcing Extensions [github.com/mholtze/akka-persistence-mongo](https://github.com/mholtze/akka-persistence-mongo).